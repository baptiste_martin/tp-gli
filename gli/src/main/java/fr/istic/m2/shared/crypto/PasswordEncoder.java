package fr.istic.m2.shared.crypto;

import com.google.gwt.user.client.Random;

public class PasswordEncoder {
	public static String encodePassword(String password) {
		byte[] binaryPassword = password.getBytes();
		byte mask = (byte) (Random.nextInt(minValue(binaryPassword) - 2) + 1);
		byte[] result = new byte[(binaryPassword.length * 4) + 1 + ((binaryPassword.length * 4) % 2)];
		result[0] = mask;
		
		int h = 1;
		for (int i = 0; i < binaryPassword.length; i++) {
			result[h++] = (byte) (binaryPassword[i] - mask);
			result[h++] = (byte) (binaryPassword[i] - mask);
			result[h++] = (byte) (binaryPassword[i] - mask);
			result[h++] = (byte) (binaryPassword[i] - mask);
		}
		
		return new String(result);
	}
	
	public static String decodePassword(String password) {
		byte[] passwordEncoded = password.getBytes();
		byte[] passwordDecoded = new byte[(passwordEncoded.length / 4) - ((passwordEncoded.length / 4) % 2)];
		byte decodeMask = passwordEncoded[0];
		
		int j = 0;
		for (int i = 1; i < passwordEncoded.length - 1; i += 4) {
			passwordDecoded[j++] = (byte) (passwordEncoded[i] + decodeMask);
		}
		
		return new String(passwordDecoded);
	}
	
	private static int minValue(byte[] array) {
		int result = array[0];
		
		for (int i = 1; i < array.length; i++) {
			if (array[i] < result) {
				result = array[i];
			}
		}
		
		return result;
	}
}
