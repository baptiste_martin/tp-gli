package fr.istic.m2.shared.model;

import java.util.List;

public interface Person {
	public String getUsername();
	public void setUsername(String username);
	public String getPassword();
	public void setPassword(String password);
	public boolean getReEncodePassword();
	public void setReEncodePassword(boolean reEncodePassword);
	public String getPhone();
	public void setPhone(String phone);
	public String getMail();
	public void setMail(String mail);
	public List<Car> getCars();
	public void setCars(List<Car> cars);
	public List<Comment> getComments();
	public void setComments(List<Comment> comments);
	public List<Trip> getTrips();
	public void setTrips(List<Trip> trips);
}
