package fr.istic.m2.shared.model;

import java.util.List;

public interface Car {
	public long getId() ;
	public void setId(long id);
	public String getModel();
	public void setModel(String model);
	public int getNbOfSeats();
	public void setNbOfSeats(int nbOfSeats);
	public Person getDriver();
	public void setDriver(Person driver);
	public List<Trip> getTrips();
	public void setTrips(List<Trip> trips);
	public boolean getHasAC();
	public void setHasAC(boolean hasAC);
	public boolean getIsSmoker();
	public void setIsSmoker(boolean isSmoker);
	public boolean getHasRadio();
	public void setHasRadio(boolean hasRadio);
}
