package fr.istic.m2.shared.model;

import java.util.List;

import fr.istic.m2.shared.model.Car;
import fr.istic.m2.shared.model.Person;

public interface Result {
	public void setPersons(List<Person> person);
	public List<Person> getPersons();
	public void setCars(List<Car> car);
	public List<Car> getCars();
    public void setTrips(List<Trip> trip);
	public List<Trip> getTrips();
    public void setComments(List<Comment> comment);
	public List<Comment> getComments();
}