package fr.istic.m2.shared.model;

public interface Comment {
	public long getId() ;
	public void setId(long id);
	public Person getPerson();
	public void setPerson(Person person);
	public Trip getTrip();
	public void setTrip(Trip trip);
	public int getRating();
	public void setRating(int rating);
	public String getComments();
	public void setComments(String comments);
}
