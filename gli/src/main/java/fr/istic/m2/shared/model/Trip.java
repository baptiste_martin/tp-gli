package fr.istic.m2.shared.model;

import java.util.Date;
import java.util.List;

public interface Trip {
	public long getId();
	public void setId(long id);
	public Car getCar();
	public void setCar(Car car);
	public List<Person> getPassengers() ;
	public void setPassengers(List<Person> passengers);
	public String getStart() ;
	public void setStart(String start) ;
	public List<String> getStops();
	public void setStops(List<String> stops);
	public String getEnd() ;
	public void setEnd(String end);
	public List<Comment> getComments();
	public void setComments(List<Comment> comments);
	public int getNbOfFreeSeats() ;
	public void setNbOfFreeSeats(int nbOfFreeSeats);
	public Date getStartDate();
	public void setStartDate(Date startDate);
}
