package fr.istic.m2.client.composite;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.istic.m2.client.Main;
import fr.istic.m2.client.Main.StatusAlert;

public abstract class AbstractComposite extends Composite {
	/**
	 * This method is call begin show page
	 */
	public abstract void showPage();
	
	/**
	 * This method is call to create page
	 */
	protected abstract void initPage();
	
	/**
	 * Show the custom message and action for popin confirm
	 * @param title Title of popin
	 * @param message Message (content) of popin
	 * @param notificationLabel Label for notification
	 * @param successMessage Message to success action
	 * @param errorMessage Message to error action
	 * @param request Request to REST service
	 */
	protected void showConfirmDialog(final String title, final String message, final String successMessage, 
			final String errorMessage, final RequestBuilder request) {
		final int WIDTH_POPIN = 200;
		final int HEIGHT_POPIN = 140;
		// Create the popin dialog box
		final DialogBox dialogBox = new DialogBox();
		final HTML msg = new HTML();
		final Button yesButton = new Button(Main.MESSAGES.yes());
		final Button noButton = new Button(Main.MESSAGES.no());
		VerticalPanel dialogVPanel = new VerticalPanel();
		
		dialogBox.setText(title);
		dialogBox.setGlassEnabled(true);
		dialogBox.setAnimationEnabled(true);
		dialogBox.setPixelSize(WIDTH_POPIN, HEIGHT_POPIN);
		dialogBox.setPopupPosition((Window.getClientWidth() - WIDTH_POPIN) / 2,
				(Window.getClientHeight() - HEIGHT_POPIN) / 2);
		
		msg.setText(message);
		
		dialogVPanel.add(msg);
		dialogVPanel.add(yesButton);
		dialogVPanel.add(noButton);
		
		dialogBox.setWidget(dialogVPanel);

		yesButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				request.setCallback(new RequestCallback() {
					public void onResponseReceived(Request request,
							Response response) {
						if (response.getStatusCode() == 204) {
							Main.addAlert(successMessage, StatusAlert.success);
						} else {
							Main.addAlert(errorMessage, StatusAlert.warning);
						}
					}

					public void onError(Request request, Throwable exception) {
						Main.addAlert("Error " + exception.getMessage(), StatusAlert.danger);
					}
				});

				try {
					request.send();
				} catch (RequestException e) {
					Main.addAlert("Error " + e.getMessage(), StatusAlert.danger);
				}
				
				dialogBox.hide();
			}
		});
		noButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				dialogBox.hide();
			}
		});

		dialogBox.show();
	}
}
