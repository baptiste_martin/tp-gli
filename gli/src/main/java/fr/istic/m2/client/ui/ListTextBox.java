package fr.istic.m2.client.ui;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.InputElement;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.ui.TextBoxBase;

public class ListTextBox extends TextBoxBase {
	private static final String CLASS_CSS_ELEMENT = "gwt-listTextBox-element";
	private static final String CLASS_CSS_INPUT_TEXT = "gwt-TextBox";
	private static final String CLASS_CSS_BUTTON_GROUP = "btn-group";
	private static final String CLASS_CSS_INPUT_BUTTON = "gwt-Button";
	
	private Element root;
	private List<InputElement> listInput;

	/**
	 * Creates root list text box.
	 */
	public ListTextBox() {
		this(Document.get().createDivElement());
	}
	
	protected ListTextBox(Element elem) {
		super(elem);
		
		listInput = new ArrayList<InputElement>();
		
		root = elem;
		root.appendChild(createWidget());
	}

	private Element createWidget() {
		return createWidget("");
	}
	
	private Element createWidget(String value) {
		final Element result = Document.get().createDivElement();
		result.addClassName(CLASS_CSS_ELEMENT);
		
		InputElement input = Document.get().createTextInputElement();
		input.setValue(value);
		input.addClassName(CLASS_CSS_INPUT_TEXT);

		Element btnGroup = Document.get().createDivElement();
		btnGroup.addClassName(CLASS_CSS_BUTTON_GROUP);
		
		InputElement addElement = Document.get().createButtonInputElement();
		addElement.setValue("+");
		addElement.addClassName(CLASS_CSS_INPUT_BUTTON);
		
		Event.sinkEvents(addElement, Event.ONCLICK);
		Event.setEventListener(addElement, new EventListener() {
			public void onBrowserEvent(Event event) {
				root.appendChild(createWidget());
			}
		});

		InputElement removeElement = Document.get().createButtonInputElement();
		if (!listInput.isEmpty()) {
			removeElement.setValue("-");
			removeElement.addClassName(CLASS_CSS_INPUT_BUTTON);
			
			
			Event.sinkEvents(removeElement, Event.ONCLICK);
			Event.setEventListener(removeElement, new EventListener() {
				public void onBrowserEvent(Event event) {
					root.removeChild(result);
				}
			});
		}
		
		btnGroup.appendChild(addElement);
		if (!listInput.isEmpty()) {
			btnGroup.appendChild(removeElement);
		}

		result.appendChild(input);
		result.appendChild(btnGroup);
		
		listInput.add(input);
		
		return result;
	}
	
	/**
	 * Get the list of values
	 * @return List of values
	 */
	public List<String> getValues() {
		List<String> result = new ArrayList<String>();
		
		for (InputElement input : listInput) {
			if (!input.getValue().equals("")) {
				result.add(input.getValue());
			}
		}
		
		return result;
	}

	/**
	 * Create the new list text box with value
	 * @return List of values for new text box
	 */
	public void setValues(List<String> values) {
		root.removeAllChildren();
		listInput.clear();
		
		for (String value : values) {
			root.appendChild(createWidget(value));
		}
	}
}
