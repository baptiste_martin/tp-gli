package fr.istic.m2.client.view.utils.observer;

public interface Observer {
	public void update();
}
