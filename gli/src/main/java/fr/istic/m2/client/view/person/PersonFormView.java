package fr.istic.m2.client.view.person;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.istic.m2.client.LoginMessages;
import fr.istic.m2.client.Main;
import fr.istic.m2.client.Main.StatusAlert;
import fr.istic.m2.client.jsonConverter.JsonConverter;
import fr.istic.m2.client.url.PageName;
import fr.istic.m2.client.url.WebScriptURL;
import fr.istic.m2.client.view.utils.HandlerBlurMandatory;
import fr.istic.m2.client.view.utils.HandlerChangeRegEx;
import fr.istic.m2.client.view.utils.HandlerClean;
import fr.istic.m2.client.view.utils.observer.Observer;
import fr.istic.m2.client.view.utils.reference.ObservableReference;
import fr.istic.m2.shared.crypto.PasswordEncoder;
import fr.istic.m2.shared.model.Person;

public class PersonFormView extends Composite {
	private final String CLASSE_CSS_PANEL = "signUpPanel";
	private final LoginMessages MESSAGES = GWT.create(LoginMessages.class);
	private FormPanel form;
	private TextBox login;
	private PasswordTextBox password;
	private PasswordTextBox confirmPassword;
	private TextBox mail;
	private TextBox confirmMail;
	private TextBox phone;
	private String username;
	private String oldPassword;
	
	public PersonFormView() {
		this(null);
	}
	
	public PersonFormView(String username) {
		this.form = new FormPanel();
		this.form.setStyleName(CLASSE_CSS_PANEL);
		this.username = username;

		initPage();
		if (username != null) fillForm();

		initWidget(this.form);
	}

	private void fillForm() {
		RequestBuilder request = 
			new RequestBuilder(RequestBuilder.GET, WebScriptURL.GET_USER.replace(":username", username));
		
		request.setCallback(new RequestCallback() {
			public void onError(Request request, Throwable exception) {
				Main.addAlert("Error " + exception.getMessage(), StatusAlert.danger);
			}

			public void onResponseReceived(Request request, Response response) {
				if (200 == response.getStatusCode()) {
					Person person = JsonConverter.getInstance().deserializePersonFromJson(response.getText());
					login.setText(person.getUsername());
					oldPassword = person.getPassword();
					mail.setText(person.getMail());
					confirmMail.setText(person.getMail());
					phone.setText(person.getPhone());
				} else {
					Main.addAlert("Error getting user (Status " + response.getStatusCode() + ")", StatusAlert.warning);
				}
			}
		});
		try {
			request.send();
		} catch (RequestException e) {
			Main.addAlert("Error " + e.getMessage(), StatusAlert.danger);
		}
	}

	private void initPage() {
		VerticalPanel panel = new VerticalPanel();
		final FlowPanel loginLine = new FlowPanel();
		final FlowPanel passwordLine = new FlowPanel();
		final FlowPanel confirmPasswordLine = new FlowPanel();
		final FlowPanel mailLine = new FlowPanel();
		final FlowPanel confirmMailLine = new FlowPanel();
		final FlowPanel phoneLine = new FlowPanel();
		
		Label title = new Label(MESSAGES.profileUpdeteTitle());
		title.setStyleName(Main.CLASSE_CSS_TITLE);
		
		final Label titleLogin = new Label(MESSAGES.lblLogin());
		login = new TextBox();
		final Label titlePassword = new Label(MESSAGES.lblPassword());
		password = new PasswordTextBox();
		final Label titleConfirmPassword = new Label(MESSAGES.confirmPassword());
		confirmPassword = new PasswordTextBox();
		final Label titleMail = new Label(MESSAGES.adressMail());
		mail = new TextBox();
		final Label titleConfirmMail = new Label(MESSAGES.confirmMail());
		confirmMail = new TextBox();
		final Label titlePhone = new Label(MESSAGES.phone());
		phone = new TextBox();
		final Button sendButton = new Button((username == null) ? MESSAGES.signUpButton()
				: MESSAGES.updateButton());

		// Default text to textBox
		login.setText(MESSAGES.defaultName());
		password.setText(MESSAGES.defaultPassword());
		confirmPassword.setText(MESSAGES.defaultPassword());
		mail.setText(MESSAGES.defaultMail());
		confirmMail.setText(MESSAGES.defaultMail());
		phone.setText(MESSAGES.defaultPhone());

		// Focus the cursor on the name field when the app loads
		login.setFocus(true);
		
		sendButton.setEnabled((username == null) ? false : true);
		
		final ObservableReference<Boolean> isLoginNotEmpty = new ObservableReference<Boolean>(false);
		final ObservableReference<Boolean> isPasswordNotEmpty = new ObservableReference<Boolean>(false);
		final ObservableReference<Boolean> isConfirmPasswordNotEmpty = new ObservableReference<Boolean>(false);
		final ObservableReference<Boolean> isMailNotEmpty = new ObservableReference<Boolean>(false);
		final ObservableReference<Boolean> isMailValide = new ObservableReference<Boolean>(false);
		final ObservableReference<Boolean> isConfirmMailNotEmpty = new ObservableReference<Boolean>(false);
		final ObservableReference<Boolean> isConfirmMailValide = new ObservableReference<Boolean>(false);
		final ObservableReference<Boolean> isPhoneNotEmpty = new ObservableReference<Boolean>(false);
		final ObservableReference<Boolean> isPhoneValide = new ObservableReference<Boolean>(false);

		Observer observer = new Observer() {			
			public void update() {
				sendButton.setEnabled(
					isLoginNotEmpty.getValue() && 
					isPasswordNotEmpty.getValue() && 
					isConfirmPasswordNotEmpty.getValue() && 
					isMailNotEmpty.getValue() && 
					isMailValide.getValue() &&
					isConfirmMailNotEmpty.getValue() && 
					isConfirmMailValide.getValue() &&
					isPhoneNotEmpty.getValue() &&
					isPhoneValide.getValue() &&
					password.getText().equals(confirmPassword.getText()) &&
					mail.getText().equals(confirmMail.getText())
				);
			}
		};
		
		isLoginNotEmpty.addObserver(observer);
		isPasswordNotEmpty.addObserver(observer);
		isConfirmPasswordNotEmpty.addObserver(observer);
		isMailNotEmpty.addObserver(observer);
		isMailValide.addObserver(observer);
		isConfirmMailNotEmpty.addObserver(observer);
		isConfirmMailValide.addObserver(observer);
		isPhoneNotEmpty.addObserver(observer);
		isPhoneValide.addObserver(observer);
		
		final String mailRegex = "[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}";
		final String phoneRegex = "([0-9]{2}( )?){4}([0-9]{2})";
		
		// Add a handler to check validity of information
		login.addBlurHandler(new HandlerBlurMandatory(login, isLoginNotEmpty));
		password.addBlurHandler(new HandlerBlurMandatory(password, isPasswordNotEmpty));
		confirmPassword.addBlurHandler(new HandlerBlurMandatory(confirmPassword, isConfirmPasswordNotEmpty));
		mail.addBlurHandler(new HandlerBlurMandatory(mail, isMailNotEmpty));
		mail.addChangeHandler(new HandlerChangeRegEx(mailRegex, mail, isMailValide));
		confirmMail.addBlurHandler(new HandlerBlurMandatory(confirmMail, isConfirmMailNotEmpty));
		confirmMail.addChangeHandler(new HandlerChangeRegEx(mailRegex, confirmMail, isConfirmMailValide));
		phone.addBlurHandler(new HandlerBlurMandatory(phone, isPhoneNotEmpty));
		phone.addBlurHandler(new BlurHandler() {
			public void onBlur(BlurEvent event) {
				if (phone.getText().matches(phoneRegex)) {
					String p = phone.getText().replaceAll(" ", "");
					phone.setText(
						p.substring(0, 2) + ' ' +
						p.substring(2, 4) + ' ' +
						p.substring(4, 6) + ' ' +
						p.substring(6, 8) + ' ' +
						p.substring(8)
					);
				}
			}
		});
		phone.addChangeHandler(new HandlerChangeRegEx(phoneRegex, phone, isPhoneValide));
		
		// Add a handler to check login on server
		sendButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Person person = JsonConverter.getInstance().makePerson();	//new Person();
				person.setUsername(login.getText());
				if (password.getText().equals(MESSAGES.defaultPassword()) || password.getText().equals("")) {
					person.setPassword(oldPassword);
					person.setReEncodePassword(false);
				} else {
					person.setPassword(PasswordEncoder.encodePassword(password.getText()));
					person.setReEncodePassword(true);
				}
				person.setMail(mail.getText());
				person.setPhone(phone.getText());
		
				RequestBuilder request = 
					(username == null) ? 
						new RequestBuilder(RequestBuilder.PUT, WebScriptURL.PUT_USER)
					:
						new RequestBuilder(RequestBuilder.POST, WebScriptURL.POST_USER);
				
				request.setHeader("Content-Type", "application/json");
				request.setRequestData(JsonConverter.getInstance().serializeToJson(person));
				request.setCallback(new RequestCallback() {
					public void onError(Request request, Throwable exception) {
						Main.addAlert("Error " + exception.getMessage(), StatusAlert.danger);
					}

					public void onResponseReceived(Request request, Response response) {
						if (200 == response.getStatusCode()) {
							Main.addAlert(((username == null) ? "Add " : "Update") + " user success", StatusAlert.success);
							Main.updateURL(PageName.USER_PAGE);
						} else {
							Main.addAlert(((username == null) ? "Add " : "Update") + 
									" user error (Status " + response.getStatusCode() + ")", StatusAlert.warning);
						}
					}
				});

				try {
					request.send();
				} catch (RequestException e) {
					Main.addAlert("Error " + e.getMessage(), StatusAlert.danger);
				}
			}
		});

		if (username == null) {
			login.addFocusHandler(new HandlerClean(MESSAGES.defaultName(), login));
			password.addFocusHandler(new HandlerClean(MESSAGES.defaultPassword(), password));		
			confirmPassword.addFocusHandler(new HandlerClean(MESSAGES.defaultPassword(), confirmPassword));		
			mail.addFocusHandler(new HandlerClean(MESSAGES.defaultMail(), mail));		
			confirmMail.addFocusHandler(new HandlerClean(MESSAGES.defaultMail(), confirmMail));		
			phone.addFocusHandler(new HandlerClean(MESSAGES.defaultPhone(), phone));
		}
		if (username == null) {
			loginLine.add(titleLogin);
			loginLine.add(login);
		} else {
			loginLine.add(login);
			login.setVisible(false);
		}

		passwordLine.add(titlePassword);
		passwordLine.add(password);

		confirmPasswordLine.add(titleConfirmPassword);
		confirmPasswordLine.add(confirmPassword);

		mailLine.add(titleMail);
		mailLine.add(mail);

		confirmMailLine.add(titleConfirmMail);
		confirmMailLine.add(confirmMail);

		phoneLine.add(titlePhone);
		phoneLine.add(phone);

		panel.add(title);
		panel.add(loginLine);
		panel.add(passwordLine);
		panel.add(confirmPasswordLine);
		panel.add(mailLine);
		panel.add(confirmMailLine);
		panel.add(phoneLine);
		panel.add(sendButton);
		
		form.add(panel);
	}
}
