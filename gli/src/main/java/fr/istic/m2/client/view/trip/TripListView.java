package fr.istic.m2.client.view.trip;

import java.util.HashMap;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

import fr.istic.m2.client.Main;
import fr.istic.m2.client.TripMessages;
import fr.istic.m2.client.Main.StatusAlert;
import fr.istic.m2.client.composite.AbstractComposite;
import fr.istic.m2.client.jsonConverter.JsonConverter;
import fr.istic.m2.client.url.PageName;
import fr.istic.m2.client.url.WebScriptURL;
import fr.istic.m2.client.view.utils.HandlerBlurMandatory;
import fr.istic.m2.client.view.utils.HandlerChangeRegEx;
import fr.istic.m2.client.view.utils.observer.Observer;
import fr.istic.m2.client.view.utils.reference.ObservableReference;
import fr.istic.m2.shared.model.Person;
import fr.istic.m2.shared.model.Trip;

public class TripListView extends AbstractComposite {
	private final String CLASSE_CSS_HEADER = "tripHeader";
	private final TripMessages MESSAGES = GWT.create(TripMessages.class);
	private FlowPanel panel;
	private FlowPanel listTrip;

	public TripListView() {
		this.panel = new FlowPanel();
		this.listTrip = new FlowPanel();
		
		initPage();		
		initWidget(panel);
	}
	
	@Override
	protected void initPage() {
		panel.add(createHeader());
		panel.add(listTrip);
	}
	
	private Widget createHeader() {
		FlowPanel header = new FlowPanel();
		header.setStyleName(CLASSE_CSS_HEADER);
		
		Label title = new Label(MESSAGES.tripTitle());
		title.setStyleName(Main.CLASSE_CSS_TITLE);
		
		FlowPanel searchPanel = new FlowPanel();
		
		FlowPanel startCityLine = new FlowPanel();
		Label startCityLabel = new Label(MESSAGES.start());
		final TextBox startCity = new TextBox();
		
		startCityLine.add(startCityLabel);
		startCityLine.add(startCity);

		FlowPanel endCityLine = new FlowPanel();
		Label endCityLabel = new Label(MESSAGES.end());
		final TextBox endCity = new TextBox();

		endCityLine.add(endCityLabel);
		endCityLine.add(endCity);

		FlowPanel dateMinLine = new FlowPanel();
		Label dateMinLabel = new Label(MESSAGES.dateMinStart());
		final DateBox dateMin = new DateBox();
		
		dateMinLine.add(dateMinLabel);
		dateMinLine.add(dateMin);

		FlowPanel dateMaxLine = new FlowPanel();
		Label dateMaxLabel = new Label(MESSAGES.dateMaxStart());
		final DateBox dateMax = new DateBox();

		FlowPanel btnGroup = new FlowPanel();
		btnGroup.setStyleName(Main.CLASSE_CSS_BTN_GROUP);
		
		final Button searchButton = new Button(MESSAGES.searchTrip());
		searchButton.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				String queryParam = "";
				
				if (dateMin.getValue() != null) {
					queryParam += "?dateMin=" + URL.encode(String.valueOf(dateMin.getValue().getTime()));
					if (dateMax.getValue() != null) {
						queryParam += "&dateMax=" + URL.encode(String.valueOf(dateMax.getValue().getTime()));
					}
				} else if (dateMax.getValue() != null) {
					queryParam += "?dateMax=" + URL.encode(String.valueOf(dateMax.getValue().getTime()));
				}
				
				RequestBuilder request = new RequestBuilder(RequestBuilder.GET, 
						WebScriptURL.GET_SEARCH_TRIP
							.replace(":start", startCity.getValue())
							.replace(":end", endCity.getValue()) + queryParam);

				request.setCallback(new RequestCallback() {
					public void onError(Request request, Throwable exception) {
						Main.addAlert("Error " + exception.getMessage(), StatusAlert.danger);
					}

					public void onResponseReceived(Request request, Response response) {
						if (response.getStatusCode() == 200) {
							listTrip.clear();
							
							List<Trip> trips = JsonConverter.getInstance().deserializeTripListFromJson(response.getText());
							for (Trip trip : trips) {
								listTrip.add(createElement(trip));
							}
						} else {
							Main.addAlert("Get trips error (Status " + response.getStatusCode() + ")", StatusAlert.warning);
						}
					}
				});

				try {
					request.send();
				} catch (RequestException e) {
					Main.addAlert("Error " + e.getMessage(), StatusAlert.danger);
				}
			}
		});
		
		final Button clearSearchButton = new Button(MESSAGES.clearSearchTrip());
		clearSearchButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				showPage();
			}
		});
		
		final ObservableReference<Boolean> isStartCityNotEmpty = new ObservableReference<Boolean>(false);
		final ObservableReference<Boolean> isStartCityValide = new ObservableReference<Boolean>(false);
		final ObservableReference<Boolean> isEndCityNotEmpty = new ObservableReference<Boolean>(false);
		final ObservableReference<Boolean> isEndCityValide = new ObservableReference<Boolean>(false);
		
		Observer observer = new Observer() {			
			public void update() {
				searchButton.setEnabled(
						isStartCityNotEmpty.getValue() &&
						isStartCityValide.getValue() &&
						isEndCityNotEmpty.getValue() &&
						isEndCityValide.getValue()
				);
			}
		};
		
		searchButton.setEnabled(false);
		
		isStartCityNotEmpty.addObserver(observer);
		isStartCityValide.addObserver(observer);
		isEndCityNotEmpty.addObserver(observer);
		isEndCityValide.addObserver(observer);
		
		String regExpCity = "([a-zA-Z -é'èùàöëê])+";
		
		// Add a handler to check validity of information
		startCity.addBlurHandler(new HandlerBlurMandatory(startCity, isStartCityNotEmpty));
		startCity.addChangeHandler(new HandlerChangeRegEx(regExpCity, startCity, isStartCityValide));
		endCity.addBlurHandler(new HandlerBlurMandatory(endCity, isEndCityNotEmpty));
		endCity.addChangeHandler(new HandlerChangeRegEx(regExpCity, endCity, isEndCityValide));
		
		dateMaxLine.add(dateMaxLabel);
		dateMaxLine.add(dateMax);
		
		searchPanel.setStyleName(Main.CLASSE_CSS_SEARCH);

		btnGroup.add(searchButton);
		btnGroup.add(clearSearchButton);
		
		searchPanel.add(startCityLine);
		searchPanel.add(endCityLine);
		searchPanel.add(dateMinLine);
		searchPanel.add(dateMaxLine);
		searchPanel.add(btnGroup);
				
		Button addTrip = new Button();
		addTrip.setHTML("<img src=\"./icon/add.svg\" title=\"Add trip\" alt=\"Add trip\" />");
		addTrip.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				Main.updateURL(PageName.ADD_TRIP_PAGE);
			}
		});

		header.add(title);
		header.add(searchPanel);
		header.add(addTrip);
		
		return header;
	}
	
	private Widget createElement(final Trip trip) {
		FlowPanel element = new FlowPanel();
		FlowPanel tripContent = new FlowPanel();
		FlowPanel buttonContent = new FlowPanel();
		Label startWidget = new Label(MESSAGES.start() + ' ' + trip.getStart());
		String stops = MESSAGES.stops() + " : ";
		for (String value : trip.getStops()) {
			stops += value + '\n';
		}
		
		Label stopsWidget = new Label(stops);
		Label endWidget = new Label(MESSAGES.end() + ' ' + trip.getEnd());
		Label nbOfFreeSeatsWidget = new Label(MESSAGES.nbOfFreeSeats() + ' ' + trip.getNbOfFreeSeats());
		Label date = new Label(MESSAGES.dateStart() + ' ' + trip.getStartDate());
		Label car = new Label(MESSAGES.car() + ' ' + trip.getCar().getModel());
		Label driver = new Label(MESSAGES.driver() + ' ' + trip.getCar().getDriver().getUsername());
		
		Button updateButton = new Button();
		updateButton.setHTML("<img src=\"./icon/update.svg\" title=\"Update trip " + trip.getId()
				+ "\" alt=\"Update trip " + trip.getId() + "\" />");
		updateButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				HashMap<String, String> parameters = new HashMap<String, String>();
				parameters.put("id", String.valueOf(trip.getId()));
				Main.updateURL(PageName.UPDATE_TRIP_PAGE, parameters);
			}
		});
		Button deleteButton = new Button();
		deleteButton.setHTML("<img src=\"./icon/delete.svg\" title=\"Delete trip " + trip.getId()
				+ "\" alt=\"Delete trip " + trip.getId() + "\" />");
		deleteButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				showConfirmDialogDelete(trip);
			}
		});
		
		Button joinButton = new Button();
		joinButton.setHTML("join");
		joinButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				showConfirmDialogJoin(trip);
			}
		});
		
		Button unjoinButton = new Button();
		unjoinButton.setHTML("unjoin");
		unjoinButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				showConfirmDialogUnJoin(trip);
			}
		});

		tripContent.setStyleName(Main.CLASSE_CSS_DIV_GROUP);
		tripContent.add(startWidget);
		tripContent.add(stopsWidget);
		tripContent.add(endWidget);
		tripContent.add(nbOfFreeSeatsWidget);
		tripContent.add(date);
		tripContent.add(car);
		tripContent.add(driver);
		
		buttonContent.setStyleName(Main.CLASSE_CSS_BTN_V_GROUP);
		
		if (trip.getCar().getDriver().getUsername().equals(Cookies.getCookie("username"))) {
			buttonContent.add(updateButton);
			buttonContent.add(deleteButton);
		} else {
			boolean isPassenger = false;
			
			if (trip.getPassengers() != null && !trip.getPassengers().isEmpty()) {
				for (Person p : trip.getPassengers()) {
					if (p.getUsername().equals(Cookies.getCookie("username"))) {
						buttonContent.add(unjoinButton);
						isPassenger = true;
						break;
					}
				}
			}
			
			if (!isPassenger) {
				buttonContent.add(joinButton);
			}
		}
		
		element.setStyleName(Main.CLASSE_CSS_ELEMENT);
		element.add(tripContent);
		element.add(buttonContent);
		
		return element;
	}

	private void showConfirmDialogJoin(Trip trip) {
		RequestBuilder request = new RequestBuilder(RequestBuilder.POST, WebScriptURL.POST_JOIN_TRIP
				.replace(":username", Cookies.getCookie("username")));
		request.setRequestData(JsonConverter.getInstance().serializeToJson(trip));
		
		super.showConfirmDialog(
				MESSAGES.confirmJoinTripTitle(),
				MESSAGES.confirmJoinTrip()
					.replaceAll("%START%", trip.getStart())
					.replaceAll("%END%", trip.getEnd()),
				MESSAGES.joinTripSuccess()
					.replaceAll("%START%", trip.getStart())
					.replaceAll("%END%", trip.getEnd()),
				MESSAGES.joinTripError()
					.replaceAll("%START%", trip.getStart())
					.replaceAll("%END%", trip.getEnd()),
				request
		);
	}
	
	private void showConfirmDialogUnJoin(Trip trip) {
		RequestBuilder request = new RequestBuilder(RequestBuilder.POST, WebScriptURL.POST_UNJOIN_TRIP
				.replace(":username", Cookies.getCookie("username")));
		request.setRequestData(JsonConverter.getInstance().serializeToJson(trip));
		
		super.showConfirmDialog(
				MESSAGES.confirmUnJoinTripTitle(),
				MESSAGES.confirmUnJoinTrip()
					.replaceAll("%START%", trip.getStart())
					.replaceAll("%END%", trip.getEnd()),
				MESSAGES.unJoinTripSuccess()
					.replaceAll("%START%", trip.getStart())
					.replaceAll("%END%", trip.getEnd()),
				MESSAGES.unJoinTripError()
					.replaceAll("%START%", trip.getStart())
					.replaceAll("%END%", trip.getEnd()),
				request
		);
	}
	
	private void showConfirmDialogDelete(Trip trip) {
		super.showConfirmDialog(
				MESSAGES.confirmDeleteTripTitle(),
				MESSAGES.confirmDeleteTrip()
					.replaceAll("%START%", trip.getStart())
					.replaceAll("%END%", trip.getEnd()),
				MESSAGES.deleteTripSuccess()
					.replaceAll("%START%", trip.getStart())
					.replaceAll("%END%", trip.getEnd()),
				MESSAGES.deleteTripError()
					.replaceAll("%START%", trip.getStart())
					.replaceAll("%END%", trip.getEnd()),
				new RequestBuilder(RequestBuilder.DELETE, 
						WebScriptURL.DELETE_TRIP
						.replace(":username", Cookies.getCookie("username"))
						.replace(":id", String.valueOf(trip.getId()))
				)
		);
	}

	@Override
	public void showPage() {
		listTrip.clear();
		
		RequestBuilder request = new RequestBuilder(RequestBuilder.GET, WebScriptURL.GET_TRIPS);
		request.setCallback(new RequestCallback() {
			public void onError(Request request, Throwable exception) {
				Main.addAlert("Error " + exception.getMessage(), StatusAlert.danger);
			}

			public void onResponseReceived(Request request, Response response) {
				if (response.getStatusCode() == 200) {
					List<Trip> trips = JsonConverter.getInstance().deserializeTripListFromJson(response.getText());
					for (Trip trip : trips) {
						listTrip.add(createElement(trip));
					}
				} else {
					Main.addAlert("Get trips error (Status " + response.getStatusCode() + ")", StatusAlert.warning);
				}
			}
		});

		try {
			request.send();
		} catch (RequestException e) {
			Main.addAlert("Error " + e.getMessage(), StatusAlert.danger);
		}
	}
}
