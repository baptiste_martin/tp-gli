package fr.istic.m2.client.view.utils.observable;

import fr.istic.m2.client.view.utils.observer.Observer;

public interface Observable {
	public void addObserver(Observer observer);
	public void removeObserver(Observer observer);
	public void updateObservers();
}
