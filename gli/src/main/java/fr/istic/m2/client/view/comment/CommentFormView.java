package fr.istic.m2.client.view.comment;

import org.cobogw.gwt.user.client.ui.Rating;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.istic.m2.client.CommentMessages;
import fr.istic.m2.client.Main;
import fr.istic.m2.client.Main.StatusAlert;
import fr.istic.m2.client.jsonConverter.JsonConverter;
import fr.istic.m2.client.url.PageName;
import fr.istic.m2.client.url.WebScriptURL;
import fr.istic.m2.client.view.utils.HandlerBlurMandatory;
import fr.istic.m2.client.view.utils.observer.Observer;
import fr.istic.m2.client.view.utils.reference.ObservableReference;
import fr.istic.m2.shared.model.Comment;
import fr.istic.m2.shared.model.Trip;

public class CommentFormView extends Composite {
	private final CommentMessages MESSAGES = GWT.create(CommentMessages.class);
	private final String CLASSE_CSS_FORM = "commentForm";
	private FormPanel form;
	private Rating rating;
	private TextArea comments;
	private String id;
	private String tripId;
	private Trip trip;
	
	public CommentFormView(String tripId) {
		this(tripId, null);
	}
	
	public CommentFormView(String tripId, String id) {
		this.form = new FormPanel();
		this.form.setStyleName(CLASSE_CSS_FORM);
		this.tripId = tripId;
		this.id = id;
		
		initPanel();
		if (id != null) fillForm();
		initWidget(form);
	}

	private void initPanel() {
		VerticalPanel panel = new VerticalPanel();
		FlowPanel ratingLine = new FlowPanel();
		FlowPanel commentLine = new FlowPanel();
		
		Label title = new Label((id == null) ? MESSAGES.commentAddTitle() : MESSAGES.commentUpdateTitle());
		title.setStyleName(Main.CLASSE_CSS_TITLE);

		Label ratingLabel = new Label(MESSAGES.rating());
		rating = new Rating(1, 5);
		final Label ratingPreview = new Label();
		
		Label commentLabel = new Label(MESSAGES.comment());
		comments = new TextArea();
		
		final Button button = new Button((id == null) ? MESSAGES.addComment() : MESSAGES.updateComment());

		button.setEnabled((id == null) ? false : true);

		final ObservableReference<Boolean> isCommentNotEmpty = new ObservableReference<Boolean>(true);
		
		Observer observer = new Observer() {			
			public void update() {
				button.setEnabled(isCommentNotEmpty.getValue());
			}
		};

		isCommentNotEmpty.addObserver(observer);

		// Add a handler to check validity of information
		comments.addBlurHandler(new HandlerBlurMandatory(comments, isCommentNotEmpty));
		
		// Add handler to show value of rating
		rating.addMouseOutHandler(new MouseOutHandler() {
			public void onMouseOut(MouseOutEvent event) {
				ratingPreview.setText(rating.getValue() + " / 5");
			}
		});
		rating.addMouseOverHandler(new MouseOverHandler() {
			public void onMouseOver(MouseOverEvent event) {
				ratingPreview.setText(rating.getHoverValue() + " / 5");
			}
		});
		
		button.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				RequestBuilder requestComment = (id == null) ?
					new RequestBuilder(RequestBuilder.PUT, 
							WebScriptURL.PUT_COMMENT.replace(":username", Cookies.getCookie("username")))
				:
					new RequestBuilder(RequestBuilder.POST, 
							WebScriptURL.POST_COMMENT.replace(":username", Cookies.getCookie("username")));
				
				Comment comment = JsonConverter.getInstance().makeComment();

				comment.setRating(rating.getValue());
				comment.setComments(comments.getText());
				
				if (id != null) {
					comment.setId(Long.valueOf(id));
					sendUpdate(requestComment, comment);
				} else {
					sendAdd(requestComment, comment);
				}
			}
		});
		
		ratingLine.add(ratingLabel);
		ratingLine.add(rating);
		ratingLine.add(ratingPreview);
		
		commentLine.add(commentLabel);
		commentLine.add(comments);
		
		panel.add(title);
		panel.add(ratingLine);
		panel.add(commentLine);
		panel.add(button);
		
		form.add(panel);
	}
	
	private void sendAdd(final RequestBuilder requestComment, final Comment comment) {
		RequestBuilder requestTrip = new RequestBuilder(RequestBuilder.GET, WebScriptURL.GET_TRIP
				.replace(":id", tripId));
		requestTrip.setHeader("Content-Type", "application/json");
		requestTrip.setCallback(new RequestCallback() {
			public void onResponseReceived(Request request,
					Response response) {
				comment.setTrip(JsonConverter.getInstance().deserializeTripFromJson(response.getText()));;
				
				requestComment.setHeader("Content-Type", "application/json");
				requestComment.setRequestData(JsonConverter.getInstance().serializeToJson(comment));
				requestComment.setCallback(new RequestCallback() {
					public void onResponseReceived(Request request, Response response) {
						if (response.getStatusCode() == 200) {
							Main.addAlert("Add comment success", StatusAlert.success);
							Main.updateURL(PageName.TRIP_PAGE);
						} else {
							Main.addAlert("Add comment error (status: " + response.getStatusCode() + ")", StatusAlert.warning);
						}
					}

					public void onError(Request request, Throwable exception) {
						Main.addAlert("Error " + exception.getMessage(), StatusAlert.danger);
					}
				});

				try {
					requestComment.send();
				} catch (RequestException e) {
					Main.addAlert("Error " + e.getMessage(), StatusAlert.danger);
				}
			}

			public void onError(Request request, Throwable exception) {
				Main.addAlert("Error " + exception.getMessage(), StatusAlert.danger);
			}
		});

		try {
			requestTrip.send();
		} catch (RequestException e) {
			Main.addAlert("Error " + e.getMessage(), StatusAlert.danger);
		}
	}
	
	private void sendUpdate(RequestBuilder requestComment, Comment comment) {
		comment.setTrip(trip);;
		
		requestComment.setHeader("Content-Type", "application/json");
		requestComment.setRequestData(JsonConverter.getInstance().serializeToJson(comment));
		requestComment.setCallback(new RequestCallback() {
			public void onResponseReceived(Request request, Response response) {
				if (response.getStatusCode() == 200) {
					Main.addAlert("Update comment success", StatusAlert.success);
					Main.updateURL(PageName.COMMENT_PAGE);
				} else {
					Main.addAlert("Update comment error (status: " + response.getStatusCode() + ")", StatusAlert.warning);
				}
			}

			public void onError(Request request, Throwable exception) {
				Main.addAlert("Error " + exception.getMessage(), StatusAlert.danger);
			}
		});

		try {
			requestComment.send();
		} catch (RequestException e) {
			Main.addAlert("Error " + e.getMessage(), StatusAlert.danger);
		}
	}
	
	private void fillForm() {
		RequestBuilder request = new RequestBuilder(RequestBuilder.GET, 
				WebScriptURL.GET_COMMENT.replace(":id", id));
		
		request.setCallback(new RequestCallback() {
			public void onResponseReceived(Request request, Response response) {
				if (response.getStatusCode() == 200) {
					Comment comment = JsonConverter.getInstance().deserializeCommentFromJson(response.getText());
					
					trip = comment.getTrip();
					rating.setValue(comment.getRating());
					comments.setText(comment.getComments());
				} else {
					Main.addAlert("Get comment error (Status " + response.getStatusText() + ")", StatusAlert.danger);
				}
			}

			public void onError(Request request, Throwable exception) {
				Main.addAlert("Error " + exception.getMessage(), StatusAlert.danger);
			}
		});

		try {
			request.send();
		} catch (RequestException e) {
			Main.addAlert("Error " + e.getMessage(), StatusAlert.danger);
		}
	}
}
