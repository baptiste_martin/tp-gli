package fr.istic.m2.client.view.utils;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.user.client.ui.TextBoxBase;

import fr.istic.m2.client.view.utils.reference.Reference;

public class HandlerBlurMandatory implements BlurHandler {
	private final static String STYLE_NAME = "mandatory";
	private final TextBoxBase WIDGET;
	private final Reference<Boolean> REFERENCE;
	
	public HandlerBlurMandatory(TextBoxBase widget, Reference<Boolean> reference) {
		WIDGET = widget;
		REFERENCE = reference;
	}
	
	public void onBlur(BlurEvent event) {
		if (WIDGET.getText().equals("")) {
			WIDGET.addStyleName(STYLE_NAME);
			REFERENCE.setValue(false);
		} else {
			WIDGET.removeStyleName(STYLE_NAME);
			REFERENCE.setValue(true);
		}
	}
}
