package fr.istic.m2.client.view.car;

import java.util.HashMap;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import fr.istic.m2.client.Main;
import fr.istic.m2.client.CarMessages;
import fr.istic.m2.client.Main.StatusAlert;
import fr.istic.m2.client.composite.AbstractComposite;
import fr.istic.m2.client.jsonConverter.JsonConverter;
import fr.istic.m2.client.url.PageName;
import fr.istic.m2.client.url.WebScriptURL;
import fr.istic.m2.shared.model.Car;

public class CarListView extends AbstractComposite {
	private final String CLASSE_CSS_HEADER = "carHeader";
	private final CarMessages MESSAGES = GWT.create(CarMessages.class);
	private FlowPanel panel;
	private FlowPanel listCar;

	public CarListView() {
		this.panel = new FlowPanel();
		this.listCar = new FlowPanel();

		initPage();
		initWidget(panel);
	}

	@Override
	protected void initPage() {
		panel.add(createHeader());
		panel.add(listCar);
	}
	
	private Widget createHeader() {
		FlowPanel header = new FlowPanel();
		header.setStyleName(CLASSE_CSS_HEADER);
		
		Label title = new Label(MESSAGES.carTitle());
				
		Button addCar = new Button();
		addCar.setHTML("<img src=\"./icon/add.svg\" title=\"Add car\" alt=\"Add car\" />");
		addCar.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				Main.updateURL(PageName.ADD_CAR_PAGE);
			}
		});
		
		title.setStyleName(Main.CLASSE_CSS_TITLE);

		header.add(title);
		header.add(addCar);
		
		return header;
	}

	private Widget createElement(final Car car) {
		FlowPanel element = new FlowPanel();
		FlowPanel carContent = new FlowPanel();
		FlowPanel buttonContent = new FlowPanel();
		Label modelWidget = new Label(MESSAGES.model() + ' ' + car.getModel());
		Label nbOfSeatsWidget = new Label(MESSAGES.numberOfSeats() + ' ' + car.getNbOfSeats());
		CheckBox hasACWidget = new CheckBox(MESSAGES.hasAC());
		hasACWidget.setValue(car.getHasAC());
		hasACWidget.setEnabled(false);
		CheckBox isSmokerWidget = new CheckBox(MESSAGES.isSmoker());
		isSmokerWidget.setValue(car.getIsSmoker());
		isSmokerWidget.setEnabled(false);
		CheckBox hasRadioWidget = new CheckBox(MESSAGES.hasRadio());
		hasRadioWidget.setValue(car.getHasRadio());
		hasRadioWidget.setEnabled(false);
		Button updateButton = new Button();
		updateButton.setHTML("<img src=\"./icon/update.svg\" title=\"Update " + car.getModel()
				+ "\" alt=\"Update " + car.getModel() + "\" />");
		updateButton.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				HashMap<String, String> parameters = new HashMap<String, String>();
				parameters.put("id", String.valueOf(car.getId()));
				Main.updateURL(PageName.UPDATE_CAR_PAGE, parameters);				
			}
		});
		Button deleteButton = new Button();
		deleteButton.setHTML("<img src=\"./icon/delete.svg\" title=\"Delete " + car.getModel()
				+ "\" alt=\"Delete " + car.getModel() + "\" />");
		deleteButton.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				showConfirmDialog(car);
			}
		});

		carContent.setStyleName(Main.CLASSE_CSS_DIV_GROUP);
		carContent.add(modelWidget);
		carContent.add(nbOfSeatsWidget);
		carContent.add(hasACWidget);
		carContent.add(isSmokerWidget);
		carContent.add(hasRadioWidget);
		
		buttonContent.setStyleName(Main.CLASSE_CSS_BTN_V_GROUP);
		buttonContent.add(updateButton);
		buttonContent.add(deleteButton);
		
		element.add(carContent);
		element.add(buttonContent);
		
		element.setStyleName(Main.CLASSE_CSS_ELEMENT);

		return element;
	}
	
	private void showConfirmDialog(final Car car) {
		super.showConfirmDialog(
				MESSAGES.confirmDeleteCarTitle(),
				MESSAGES.confirmDeleteCar().replaceAll("%NAME%", car.getModel()),
				MESSAGES.deleteCarSuccess().replace("%NAME%", car.getModel()),
				MESSAGES.deleteCarError().replace("%NAME%", car.getModel()),
				new RequestBuilder(RequestBuilder.DELETE, 
						WebScriptURL.DELETE_CAR.replace(":id", String.valueOf(car.getId()))
				)
		);
	}

	@Override
	public void showPage() {
		listCar.clear();
		
		RequestBuilder request = new RequestBuilder(RequestBuilder.GET, 
				WebScriptURL.GET_CARS.replace(":username", Cookies.getCookie("username")));
		request.setCallback(new RequestCallback() {
			public void onError(Request request, Throwable exception) {
				Main.addAlert("Error " + exception.getMessage(), StatusAlert.danger);
			}

			public void onResponseReceived(Request request, Response response) {
				if (200 == response.getStatusCode()) {
					List<Car> cars = JsonConverter.getInstance().deserializeCarListFromJson(response.getText());
					for (Car car : cars) {
						listCar.add(createElement(car));
					}
				} else {
					Main.addAlert("Error getting cars (Status " + response.getStatusCode() + ")", StatusAlert.danger);
				}
			}
		});

		try {
			request.send();
		} catch (RequestException e) {
			Main.addAlert("Error " + e.getMessage(), StatusAlert.danger);
		}
	}
}
