package fr.istic.m2.client.view.comment;

import java.util.HashMap;
import java.util.List;

import org.cobogw.gwt.user.client.ui.Rating;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

import fr.istic.m2.client.CommentMessages;
import fr.istic.m2.client.Main;
import fr.istic.m2.client.Main.StatusAlert;
import fr.istic.m2.client.composite.AbstractComposite;
import fr.istic.m2.client.jsonConverter.JsonConverter;
import fr.istic.m2.client.url.PageName;
import fr.istic.m2.client.url.WebScriptURL;
import fr.istic.m2.client.view.utils.HandlerBlurMandatory;
import fr.istic.m2.client.view.utils.HandlerChangeRegEx;
import fr.istic.m2.client.view.utils.observer.Observer;
import fr.istic.m2.client.view.utils.reference.ObservableReference;
import fr.istic.m2.shared.model.Comment;
import fr.istic.m2.shared.model.Trip;

public class CommentListView extends AbstractComposite {
	private final String CLASSE_CSS_HEADER = "commentHeader";
	private final CommentMessages MESSAGES = GWT.create(CommentMessages.class);
	private FlowPanel panel;
	private FlowPanel listTrip;

	public CommentListView() {
		this.panel = new FlowPanel();
		this.listTrip = new FlowPanel();
		
		initPage();		
		initWidget(panel);
	}
	
	@Override
	protected void initPage() {
		panel.add(createHeader());
		panel.add(listTrip);
	}
	
	private Widget createHeader() {
		FlowPanel header = new FlowPanel();
		header.setStyleName(CLASSE_CSS_HEADER);
		
		Label title = new Label(MESSAGES.commentTitle());
		title.setStyleName(Main.CLASSE_CSS_TITLE);
		
		FlowPanel searchPanel = new FlowPanel();
		
		FlowPanel startCityLine = new FlowPanel();
		Label startCityLabel = new Label(MESSAGES.start());
		final TextBox startCity = new TextBox();
		
		startCityLine.add(startCityLabel);
		startCityLine.add(startCity);

		FlowPanel endCityLine = new FlowPanel();
		Label endCityLabel = new Label(MESSAGES.end());
		final TextBox endCity = new TextBox();

		endCityLine.add(endCityLabel);
		endCityLine.add(endCity);

		FlowPanel dateMinLine = new FlowPanel();
		Label dateMinLabel = new Label(MESSAGES.dateMinStart());
		final DateBox dateMin = new DateBox();
		
		dateMinLine.add(dateMinLabel);
		dateMinLine.add(dateMin);

		FlowPanel dateMaxLine = new FlowPanel();
		Label dateMaxLabel = new Label(MESSAGES.dateMaxStart());
		final DateBox dateMax = new DateBox();
		
		FlowPanel btnGroup = new FlowPanel();
		btnGroup.setStyleName(Main.CLASSE_CSS_BTN_GROUP);
		
		final Button searchButton = new Button(MESSAGES.searchTrip());
		searchButton.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				String queryParam = "";
				
				if (dateMin.getValue() != null) {
					queryParam += "?dateMin=" + URL.encode(String.valueOf(dateMin.getValue().getTime()));
					if (dateMax.getValue() != null) {
						queryParam += "&dateMax=" + URL.encode(String.valueOf(dateMax.getValue().getTime()));
					}
				} else if (dateMax.getValue() != null) {
					queryParam += "?dateMax=" + URL.encode(String.valueOf(dateMax.getValue().getTime()));
				}
				
				RequestBuilder request = new RequestBuilder(RequestBuilder.GET, 
						WebScriptURL.GET_SEARCH_TRIP_WITH_USERNAME
							.replace(":username", Cookies.getCookie("username"))
							.replace(":start", startCity.getValue())
							.replace(":end", endCity.getValue()) + queryParam);

				request.setCallback(new RequestCallback() {
					public void onError(Request request, Throwable exception) {
						Main.addAlert("Error " + exception.getMessage(), StatusAlert.danger);
					}

					public void onResponseReceived(Request request, Response response) {
						if (response.getStatusCode() == 200) {
							listTrip.clear();
							
							List<Trip> trips = JsonConverter.getInstance().deserializeTripListFromJson(response.getText());
							for (Trip trip : trips) {
								listTrip.add(createElement(trip));
							}
						} else {
							Main.addAlert("Error get old trip (status" + response.getStatusCode() + ")", StatusAlert.warning);
						}
					}
				});

				try {
					request.send();
				} catch (RequestException e) {
					Main.addAlert("Error " + e.getMessage(), StatusAlert.danger);
				}
			}
		});
		
		final Button clearSearchButton = new Button(MESSAGES.clearSearchTrip());
		clearSearchButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				showPage();
			}
		});
		
		final ObservableReference<Boolean> isStartCityNotEmpty = new ObservableReference<Boolean>(false);
		final ObservableReference<Boolean> isStartCityValide = new ObservableReference<Boolean>(false);
		final ObservableReference<Boolean> isEndCityNotEmpty = new ObservableReference<Boolean>(false);
		final ObservableReference<Boolean> isEndCityValide = new ObservableReference<Boolean>(false);
		
		Observer observer = new Observer() {			
			public void update() {
				searchButton.setEnabled(
						isStartCityNotEmpty.getValue() &&
						isStartCityValide.getValue() &&
						isEndCityNotEmpty.getValue() &&
						isEndCityValide.getValue()
				);
			}
		};
		
		searchButton.setEnabled(false);
		
		isStartCityNotEmpty.addObserver(observer);
		isStartCityValide.addObserver(observer);
		isEndCityNotEmpty.addObserver(observer);
		isEndCityValide.addObserver(observer);
		
		String regExpCity = "([a-zA-Z -é'èùàöëê])+";
		
		// Add a handler to check validity of information
		startCity.addBlurHandler(new HandlerBlurMandatory(startCity, isStartCityNotEmpty));
		startCity.addChangeHandler(new HandlerChangeRegEx(regExpCity, startCity, isStartCityValide));
		endCity.addBlurHandler(new HandlerBlurMandatory(endCity, isEndCityNotEmpty));
		endCity.addChangeHandler(new HandlerChangeRegEx(regExpCity, endCity, isEndCityValide));
		
		dateMaxLine.add(dateMaxLabel);
		dateMaxLine.add(dateMax);

		searchPanel.setStyleName(Main.CLASSE_CSS_SEARCH);

		btnGroup.add(searchButton);
		btnGroup.add(clearSearchButton);
		
		searchPanel.add(startCityLine);
		searchPanel.add(endCityLine);
		searchPanel.add(dateMinLine);
		searchPanel.add(dateMaxLine);
		searchPanel.add(btnGroup);
		
		header.add(title);
		header.add(searchPanel);
		
		return header;
	}
	
	private Widget createElement(final Trip trip) {
		FlowPanel element = new FlowPanel();
		final FlowPanel child = new FlowPanel();
		Label startWidget = new Label(MESSAGES.start() + ' ' + trip.getStart());
		String stops = MESSAGES.stops() + " : ";
		for (String value : trip.getStops()) {
			stops += value + '\n';
		}
		
		Label stopsWidget = new Label(stops);
		Label endWidget = new Label(MESSAGES.end() + ' ' + trip.getEnd());
		Label nbOfFreeSeatsWidget = new Label(MESSAGES.nbOfFreeSeats() + ' ' + trip.getNbOfFreeSeats());
		Label date = new Label(MESSAGES.dateStart() + ' ' + trip.getStartDate());
		Label car = new Label(MESSAGES.car() + ' ' + trip.getCar().getModel());
		Label driver = new Label(MESSAGES.driver() + ' ' + trip.getCar().getDriver().getUsername());
		
		Button showButton = new Button(MESSAGES.showComment());
		showButton.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				toogleComment(child);
			}
		});
		
		Button addButton = new Button();
		addButton.setHTML("<img src=\"./icon/add.svg\" title=\"Add comment with trip " + trip.getId() + 
				"\" alt=\"Add comment with trip " + trip.getId() + "\" />");
		addButton.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				HashMap<String, String> parameters = new HashMap<String, String>();
				parameters.put("tripId", String.valueOf(trip.getId()));
				Main.updateURL(PageName.UPDATE_COMMENT_PAGE, parameters);
			}
		});

		boolean isCommented = false;
		
		for (final Comment comment : trip.getComments()) {
			FlowPanel commentElement = new FlowPanel();
			FlowPanel divGroup = new FlowPanel();
			FlowPanel btnGroup = new FlowPanel();
			
			Rating ratingWidget = new Rating(1, 5);
			ratingWidget.setValue(comment.getRating());
			ratingWidget.setReadOnly(true);
			Label commentWidget = new Label(comment.getComments());
			Button updateButton = new Button();
			updateButton.setHTML("<img src=\"./icon/update.svg\" title=\"Update comment " + comment.getId()
					+ "\" alt=\"Update trip " + trip.getId() + "\" />");
			updateButton.addClickHandler(new ClickHandler() {			
				public void onClick(ClickEvent event) {
					HashMap<String, String> parameters = new HashMap<String, String>();
					parameters.put("tripId", String.valueOf(trip.getId()));
					parameters.put("id", String.valueOf(comment.getId()));
					Main.updateURL(PageName.UPDATE_COMMENT_PAGE, parameters);
				}
			});
			Button deleteButton = new Button();
			deleteButton.setHTML("<img src=\"./icon/delete.svg\" title=\"Delete comment " + comment.getId()
					+ "\" alt=\"Delete trip " + trip.getId() + "\" />");
			deleteButton.addClickHandler(new ClickHandler() {			
				public void onClick(ClickEvent event) {
					showConfirmDialogDelete(comment);
				}
			});
			
			divGroup.add(ratingWidget);
			divGroup.add(commentWidget);
			
			btnGroup.add(updateButton);
			btnGroup.add(deleteButton);
			
			commentElement.add(divGroup);
			
			if (comment.getPerson().getUsername().equals(Cookies.getCookie("username"))) {
				isCommented = true;
				commentElement.add(btnGroup);
			}
			
			divGroup.setStyleName(Main.CLASSE_CSS_DIV_GROUP);
			btnGroup.setStyleName(Main.CLASSE_CSS_BTN_V_GROUP);
			commentElement.setStyleName(Main.CLASSE_CSS_ELEMENT);
			
			child.add(commentElement);
		}
		
		child.setVisible(false);
		
		element.add(startWidget);
		element.add(stopsWidget);
		element.add(endWidget);
		element.add(nbOfFreeSeatsWidget);
		element.add(date);
		element.add(car);
		element.add(driver);
		if (!isCommented) {
			element.add(addButton);
		}
		element.add(showButton);
		element.add(child);
		
		element.setStyleName(Main.CLASSE_CSS_ELEMENT);
		
		return element;
	}
	
	private void setComments(List<Trip> trips) {
		for (final Trip trip : trips) {
			RequestBuilder requestComment = 
					new RequestBuilder(RequestBuilder.GET, 
							WebScriptURL.GET_COMMENT_WITH_TRIP.replace(":id", trip.getId() + ""));
			requestComment.setCallback(new RequestCallback() {
				public void onError(Request request, Throwable exception) {
					Main.addAlert("Error " + exception.getMessage(), StatusAlert.danger);
				}

				public void onResponseReceived(Request request, Response response) {
					if (response.getStatusCode() == 200) {
						List<Comment> comments = 
								JsonConverter
									.getInstance()
									.deserializeCommentListFromJson(response.getText());
						trip.setComments(comments);
						listTrip.add(createElement(trip));
					} else {
						Main.addAlert("Error get comments (Status " + response.getStatusCode() + ")", StatusAlert.warning);
					}
				}
			});

			try {
				requestComment.send();
			} catch (RequestException e) {
				Main.addAlert("Error " + e.getMessage(), StatusAlert.danger);
			}
		}
	}
	
	private void toogleComment(FlowPanel commentList) {
		commentList.setVisible(!commentList.isVisible());
	}

	private void showConfirmDialogDelete(Comment comment) {
		super.showConfirmDialog(
				MESSAGES.confirmDeleteCommentTitle(),
				MESSAGES.confirmDeleteComment()
					.replaceAll("%TEXT%", comment.getComments()),
				MESSAGES.deleteCommentSuccess(),
				MESSAGES.deleteCommentError(),
				new RequestBuilder(RequestBuilder.DELETE, 
						WebScriptURL.DELETE_COMMENT
						.replace(":username", Cookies.getCookie("username"))
						.replace(":id", String.valueOf(comment.getId()))
				)
		);
	}

	@Override
	public void showPage() {
		listTrip.clear();
		
		RequestBuilder request = new RequestBuilder(RequestBuilder.GET, WebScriptURL.GET_TRIPS_WITH_USERNAME
				.replace(":username", Cookies.getCookie("username")));
		request.setCallback(new RequestCallback() {
			public void onError(Request request, Throwable exception) {
				Main.addAlert("Error " + exception.getMessage(), StatusAlert.danger);
			}

			public void onResponseReceived(Request request, Response response) {
				if (response.getStatusCode() == 200) {
					List<Trip> trips = JsonConverter.getInstance().deserializeTripListFromJson(response.getText());
					
					setComments(trips);
				} else {
					Main.addAlert("Error getting previous trips (Status " + response.getStatusCode() + ")", StatusAlert.warning);
				}
			}
		});

		try {
			request.send();
		} catch (RequestException e) {
			Main.addAlert("Error " + e.getMessage(), StatusAlert.danger);
		}
	}
}
