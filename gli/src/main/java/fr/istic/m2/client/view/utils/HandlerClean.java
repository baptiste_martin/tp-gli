package fr.istic.m2.client.view.utils;

import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.user.client.ui.TextBoxBase;

public class HandlerClean implements FocusHandler {
	private final String DEFAULT_VALUE;
	private final TextBoxBase WIDGET;
	
	public HandlerClean(String defaultValue, TextBoxBase widget) {
		DEFAULT_VALUE = defaultValue;
		WIDGET = widget;
	}
	
	public void onFocus(FocusEvent event) {
		if (this.WIDGET.getText().equals(this.DEFAULT_VALUE)) {
			this.WIDGET.setText("");
		}
	}
}