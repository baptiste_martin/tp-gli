package fr.istic.m2.client;

import java.util.HashMap;
import java.util.Iterator;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;

import fr.istic.m2.client.url.PageName;
import fr.istic.m2.client.view.MainView;
import fr.istic.m2.client.view.car.CarFormView;
import fr.istic.m2.client.view.car.CarListView;
import fr.istic.m2.client.view.comment.CommentFormView;
import fr.istic.m2.client.view.comment.CommentListView;
import fr.istic.m2.client.view.login.LoginView;
import fr.istic.m2.client.view.person.PersonFormView;
import fr.istic.m2.client.view.person.PersonView;
import fr.istic.m2.client.view.trip.TripFormView;
import fr.istic.m2.client.view.trip.TripListView;

public class Main implements EntryPoint {
	public static enum StatusAlert { success, warning, danger }
	public static final Messages MESSAGES = GWT.create(Messages.class);
	public static final String CLASSE_CSS_TITLE = "title";
	public static final String CLASSE_CSS_ELEMENT = "listElement";
	public static final String CLASSE_CSS_SEARCH = "searchHeader";
	public static final String CLASSE_CSS_DIV_GROUP = "div-group";
	public static final String CLASSE_CSS_BTN_GROUP = "btn-group";
	public static final String CLASSE_CSS_BTN_V_GROUP = "btn-group-vertical";
	private static MainView mainView;
	private static LoginView loginView;
	public static String entryToken;
	public static Button disconnect;
	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		mainView = new MainView();
		loginView = new LoginView();
		entryToken = "";
		disconnect = new Button(MESSAGES.disconnect());
		disconnect.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Cookies.removeCookie("username");
				updateURL(PageName.LOGIN_PAGE);
			}
		});

		initApp();		
		initHistory();
		
		RootPanel.get("title").add(new Label(MESSAGES.title()));
		
		if (Cookies.getCookie("username") == null) {
			if (History.getToken().equals(PageName.SIGNUP_PAGE)) {
				updateURL(PageName.SIGNUP_PAGE);
			} else {
				entryToken = History.getToken();
				updateURL(PageName.LOGIN_PAGE);
			}
		} else {
			if (History.getToken().equals("")) {
				updateURL(PageName.USER_PAGE);
			}
		}
		
		History.fireCurrentHistoryState();
	}

	private void initApp() {
		mainView.addPage(PageName.USER_PAGE, new PersonView());
		mainView.addPage(PageName.CAR_PAGE, new CarListView());
		mainView.addPage(PageName.TRIP_PAGE, new TripListView());
		mainView.addPage(PageName.COMMENT_PAGE, new CommentListView());
		
		mainView.validatePage(Window.getClientWidth() - 40);
	}
		
	private void initHistory() {
		History.addValueChangeHandler(new ValueChangeHandler<String>() {
			public void onValueChange(ValueChangeEvent<String> event) {
				String historyToken = event.getValue();
				
				if (Cookies.getCookie("username") != null) {
					RootPanel.get("disconnect").add(disconnect);
					
					if (historyToken.equals(PageName.USER_PAGE)) {
						mainView.selectPage(PageName.USER_PAGE);
						changePage(mainView);
						return;
					} else if (historyToken.equals(PageName.UPDATE_USER_PAGE)) {
						changePage(new PersonFormView(Cookies.getCookie("username")));
						return;
					} else if (historyToken.equals(PageName.CAR_PAGE)) {
						mainView.selectPage(PageName.CAR_PAGE);
						changePage(mainView);
						return;
					} else if (historyToken.equals(PageName.ADD_CAR_PAGE)) {
						changePage(new CarFormView());
						return;
					} else if (historyToken.startsWith(PageName.UPDATE_CAR_PAGE)) {
						HashMap<String, String> parameters = decodeParameters(historyToken);
						changePage(new CarFormView(parameters.get("id")));
						return;				
					} else if (historyToken.equals(PageName.TRIP_PAGE)) {
						mainView.selectPage(PageName.TRIP_PAGE);
						changePage(mainView);
						return;
					} else if (historyToken.equals(PageName.ADD_TRIP_PAGE)) {
						changePage(new TripFormView());
						return;
					} else if (historyToken.startsWith(PageName.UPDATE_TRIP_PAGE)) {
						HashMap<String, String> parameters = decodeParameters(historyToken);
						changePage(new TripFormView(parameters.get("id")));
						return;
					} else if (historyToken.equals(PageName.COMMENT_PAGE)) {
						mainView.selectPage(PageName.COMMENT_PAGE);
						changePage(mainView);
						return;
					} else if (historyToken.equals(PageName.ADD_COMMENT_PAGE)) {
						HashMap<String, String> parameters = decodeParameters(historyToken);
						changePage(new CommentFormView(parameters.get("tripId")));
						return;
					} else if (historyToken.startsWith(PageName.UPDATE_COMMENT_PAGE)) {
						HashMap<String, String> parameters = decodeParameters(historyToken);
						changePage(new CommentFormView(parameters.get("tripId"), parameters.get("id")));
						return;
					}
				}
				
				RootPanel.get("disconnect").clear();
				
				if (historyToken.equals(PageName.SIGNUP_PAGE)) {
					changePage(new PersonFormView());
					return;
				}
				
				changePage(loginView);
			}
		});
	}
	
	private HashMap<String, String> decodeParameters(String path) {
		HashMap<String, String> parameters = new HashMap<String, String>();
		
		for(String element : path.split("_")) {
			String[] idValue = element.split("=");
			if (idValue.length > 1 && idValue.length <= 2) {
				parameters.put(idValue[0], idValue[1]);
			}
		}
		
		return parameters;
	}
	
	private void changePage(Composite view) {
		RootPanel.get("content").clear();
		RootPanel.get("content").add(view);
	}
	
	public static void updateURL(String path) {
		History.newItem(path);
	}
	
	public static void updateURL(String path, HashMap<String, String> parameters) {
		String pathWithParameters = path;
		
		Iterator<String> it = parameters.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			pathWithParameters += '_' + key + '=' + parameters.get(key); 
		}
		
		History.newItem(pathWithParameters);
	}
	
	public static void addAlert(String msg, StatusAlert status) {
		final FlowPanel panel = new FlowPanel();
		panel.setStyleName("alert " + status);
		
		Label alert = new Label(msg);
		Anchor closeAlert = new Anchor("x");
		
		alert.setStyleName("alertMsg");
		
		// Create a new timer that closes the alert.
		Timer t = new Timer() {
			@Override
			public void run() {
				RootPanel.get("listAlert").remove(panel);
			}
		};
		
		closeAlert.setStyleName("closeAlert");
		closeAlert.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				RootPanel.get("listAlert").remove(panel);
			}
		});
		
		panel.add(alert);
		panel.add(closeAlert);
		
		RootPanel.get("listAlert").add(panel);

		// Schedule the timer to run 10 seconds later.
		t.schedule(10000);
	}
}
