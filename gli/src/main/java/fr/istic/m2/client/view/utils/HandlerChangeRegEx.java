package fr.istic.m2.client.view.utils;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.TextBoxBase;

import fr.istic.m2.client.view.utils.reference.Reference;

public class HandlerChangeRegEx implements ChangeHandler {
	private final static String STYLE_NAME = "error";
	private final String REGEX;
	private final TextBoxBase WIDGET;
	private final Reference<Boolean> REFERENCE;
	
	public HandlerChangeRegEx(String regEx, TextBoxBase widget, Reference<Boolean> reference) {
		REGEX = regEx;
		WIDGET = widget;
		REFERENCE = reference;
	}

	public void onChange(ChangeEvent event) {
		if (WIDGET.getText().matches(REGEX)) {
			WIDGET.removeStyleName(STYLE_NAME);
			REFERENCE.setValue(true);
		} else {
			WIDGET.addStyleName(STYLE_NAME);
			REFERENCE.setValue(false);
		}
	}
}
