package fr.istic.m2.client.view.login;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.istic.m2.client.LoginMessages;
import fr.istic.m2.client.Main;
import fr.istic.m2.client.Main.StatusAlert;
import fr.istic.m2.client.jsonConverter.JsonConverter;
import fr.istic.m2.client.url.PageName;
import fr.istic.m2.client.url.WebScriptURL;
import fr.istic.m2.client.view.utils.HandlerClean;
import fr.istic.m2.shared.crypto.PasswordEncoder;
import fr.istic.m2.shared.model.Person;

public class LoginView extends Composite {
	private final String CLASSE_CSS_PANEL = "loginPanel";
	private final LoginMessages MESSAGES = GWT.create(LoginMessages.class);
	private FormPanel form;

	public LoginView() {
		form = new FormPanel();
		form.setStyleName(CLASSE_CSS_PANEL);

		initPage();

		initWidget(form);
	}

	public void initPage() {
		VerticalPanel panel = new VerticalPanel();
		
		final FlowPanel loginLine = new FlowPanel();
		final FlowPanel passwordLine = new FlowPanel();
		final FlowPanel buttonLine = new FlowPanel();
		final Label titleLogin = new Label(MESSAGES.lblLogin());
		final TextBox login = new TextBox();
		final Label titlePassword = new Label(MESSAGES.lblPassword());
		final PasswordTextBox password = new PasswordTextBox();
		final Button sendButton = new Button(MESSAGES.sendButton());
		final Button signUpButton = new Button(MESSAGES.signUpButton());

		// Default text to textBox
		login.setText(MESSAGES.defaultName());
		password.setText(MESSAGES.defaultPassword());

		// Focus the cursor on the name field when the app loads
		login.setFocus(true);

		// Add a handler to check login on server
		sendButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				checkConnection(login.getText(), password.getText());
			}
		});
		
		signUpButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Main.updateURL(PageName.SIGNUP_PAGE);
			}
		});

		login.addFocusHandler(new HandlerClean(MESSAGES.defaultName(), login));		
		login.addKeyUpHandler(new KeyUpHandler() {
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					checkConnection(login.getText(), password.getText());
				}
			}
		});

		password.addFocusHandler(new HandlerClean(MESSAGES.defaultPassword(), password));		
		password.addKeyUpHandler(new KeyUpHandler() {
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					checkConnection(login.getText(), password.getText());
				}
			}
		});

		loginLine.add(titleLogin);
		loginLine.add(login);

		passwordLine.add(titlePassword);
		passwordLine.add(password);

		buttonLine.add(sendButton);
		buttonLine.add(signUpButton);
		buttonLine.setStyleName(Main.CLASSE_CSS_BTN_GROUP);

		panel.add(loginLine);
		panel.add(passwordLine);
		panel.add(buttonLine);
		
		form.add(panel);
	}

	private void checkConnection(final String login, String mdp) {
		RequestBuilder request = new RequestBuilder(RequestBuilder.POST, WebScriptURL.POST_LOGIN);

		Person person = JsonConverter.getInstance().makePerson();//new Person();

		person.setUsername(login);
		person.setPassword(PasswordEncoder.encodePassword(mdp));

		request.setHeader("Content-Type", "application/json");
		request.setRequestData(JsonConverter.getInstance().serializeToJson(person));
		request.setCallback(new RequestCallback() {
			public void onError(Request request, Throwable exception) {
				Main.addAlert(MESSAGES.errorServer(), StatusAlert.danger);
			}

			public void onResponseReceived(Request request, Response response) {
				if (200 == response.getStatusCode()) {
					if (response.getText().equals("true")) {
						Main.addAlert(MESSAGES.loginSuccess(), StatusAlert.success);
						Cookies.setCookie("username", login);
						showDialog(login);
					} else {
						Main.addAlert(MESSAGES.authError(), StatusAlert.warning);
					}
				} else {
					Main.addAlert(MESSAGES.errorServer(), StatusAlert.warning);
				}
			}
		});

		try {
			request.send();
		} catch (RequestException e) {
			Main.addAlert(MESSAGES.errorServer(), StatusAlert.danger);
		}
	}

	private void showDialog(String username) {
		final int WIDTH_POPIN = 200;
		final int HEIGHT_POPIN = 90;
		// Create the popin dialog box
		final DialogBox dialogBox = new DialogBox();
		final HTML msg = new HTML();
		final Button closeButton = new Button(MESSAGES.close());
		VerticalPanel dialogVPanel = new VerticalPanel();

		dialogBox.setText(MESSAGES.popinTitle());
		dialogBox.setGlassEnabled(true);
		dialogBox.setAnimationEnabled(true);
		dialogBox.setPixelSize(WIDTH_POPIN, HEIGHT_POPIN);
		dialogBox.setPopupPosition((Window.getClientWidth() - WIDTH_POPIN) / 2,
				(Window.getClientHeight() - HEIGHT_POPIN) / 2);

		msg.setText(MESSAGES.hello() + ' ' + username + ".");

		dialogVPanel.add(msg);
		dialogVPanel.add(closeButton);

		dialogBox.setWidget(dialogVPanel);

		// Add a handler to close the DialogBox
		closeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				dialogBox.hide();
				Main.updateURL((Main.entryToken.equals("")) ? PageName.USER_PAGE : Main.entryToken);
			}
		});
		dialogBox.show();
	}
}
