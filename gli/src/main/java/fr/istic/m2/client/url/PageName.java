package fr.istic.m2.client.url;

/**
 * Interface management application page name
 */
public interface PageName {
	// Login page
	public static final String LOGIN_PAGE = "/login";
	
	// SignUp page
	public static final String SIGNUP_PAGE = "/signUp";

	// User section
	public static final String USER_PAGE = "/user";
	public static final String UPDATE_USER_PAGE = "/user/update";

	// Car section
	public static final String CAR_PAGE = "/car";
	public static final String ADD_CAR_PAGE = "/car/add";
	public static final String UPDATE_CAR_PAGE = "/car/update";
	
	// Trip section
	public static final String TRIP_PAGE = "/trip";
	public static final String ADD_TRIP_PAGE = "/trip/add";
	public static final String UPDATE_TRIP_PAGE = "/trip/update";
	
	// Comment section
	public static final String COMMENT_PAGE = "/comment";
	public static final String ADD_COMMENT_PAGE = "/comment/add";
	public static final String UPDATE_COMMENT_PAGE = "/comment/update";
}
