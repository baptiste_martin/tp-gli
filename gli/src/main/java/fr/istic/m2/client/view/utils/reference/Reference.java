package fr.istic.m2.client.view.utils.reference;

public class Reference<T> {
	private T value;
	
	public T getValue() {
		return value;
	}
	
	public void setValue(T value) {
		this.value = value;
	}
	
	public Reference() {
		this.value = null;
	}
	
	public Reference(T value) {
		this.value = value;
	}
}
