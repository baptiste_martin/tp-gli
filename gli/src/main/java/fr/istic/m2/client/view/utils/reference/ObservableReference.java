package fr.istic.m2.client.view.utils.reference;

import java.util.ArrayList;
import java.util.List;

import fr.istic.m2.client.view.utils.observable.Observable;
import fr.istic.m2.client.view.utils.observer.Observer;


public class ObservableReference<T> extends Reference<T> implements Observable {
	private List<Observer> observers;

	public ObservableReference() {
		super();
	}
	
	public ObservableReference(T value) {
		super(value);
		observers = new ArrayList<Observer>();
	}
	
	@Override
	public void setValue(T value) {
		super.setValue(value);
		updateObservers();
	}

	public void addObserver(Observer observer) {
		observers.add(observer);
	}

	public void removeObserver(Observer observer) {
		observers.remove(observer);
	}

	public void updateObservers() {
		for (Observer o : observers) {
			o.update();
		}
	}
}
