package fr.istic.m2.client.view.person;

import java.util.HashMap;
import java.util.List;

import org.cobogw.gwt.user.client.ui.Rating;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;

import fr.istic.m2.client.LoginMessages;
import fr.istic.m2.client.Main;
import fr.istic.m2.client.Main.StatusAlert;
import fr.istic.m2.client.composite.AbstractComposite;
import fr.istic.m2.client.jsonConverter.JsonConverter;
import fr.istic.m2.client.url.PageName;
import fr.istic.m2.client.url.WebScriptURL;
import fr.istic.m2.shared.model.Comment;
import fr.istic.m2.shared.model.Person;

public class PersonView extends AbstractComposite {
	private final LoginMessages MESSAGES = GWT.create(LoginMessages.class);
	private FlowPanel panel;
	private FlowPanel header;
	private FlowPanel content;
	private FlowPanel commentPanel;
	private Label nameValue;
	private Label mailValue;
	private Label phoneValue;
	Person person = null;

	public PersonView() {
		this.panel = new FlowPanel();
		this.header = new FlowPanel();
		this.content = new FlowPanel();
		this.commentPanel = new FlowPanel();

		initPage();

		this.panel.add(this.header);
		this.panel.add(this.content);

		initWidget(this.panel);
	}
	
	@Override
	protected void initPage() {
		content.setStyleName(Main.CLASSE_CSS_ELEMENT);
		
		Label title = new Label(MESSAGES.profileTitle());
		title.setStyleName(Main.CLASSE_CSS_TITLE);
		
		final Label nameLabel = new Label(MESSAGES.lblLogin());
		nameValue = new Label();
		final Label mailLabel = new Label(MESSAGES.adressMail());
		mailValue = new Label();
		final Label phoneLabel = new Label(MESSAGES.phone());
		phoneValue = new Label();
		final Button updatePerson = new Button("<img src=\"./icon/update.svg\" title=\"Update profile\""
				+ " alt=\"Update profile\" />");
		
		updatePerson.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				Main.updateURL(PageName.UPDATE_USER_PAGE);
			}
		});
		
		Button showComment = new Button(MESSAGES.showComment());
		showComment.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				commentPanel.setVisible(!commentPanel.isVisible());
			}
		});
		
		commentPanel.setVisible(false);
		
		header.add(updatePerson);
		
		content.add(title);
		content.add(nameLabel);
		content.add(nameValue);
		content.add(mailLabel);
		content.add(mailValue);
		content.add(phoneLabel);
		content.add(phoneValue);
		content.add(showComment);
		content.add(commentPanel);
	}

	@Override
	public void showPage() {
		RequestBuilder request = new RequestBuilder(RequestBuilder.GET,
				WebScriptURL.GET_USER.replace(":username", Cookies.getCookie("username")));
		request.setCallback(new RequestCallback() {
			public void onResponseReceived(Request request, Response response) {
				if (200 == response.getStatusCode()) {
					person = JsonConverter.getInstance().deserializePersonFromJson(response.getText());
					nameValue.setText(person.getUsername());
					mailValue.setText(person.getMail());
					phoneValue.setText(person.getPhone());
					
					RequestBuilder requestComment = new RequestBuilder(RequestBuilder.GET,
							WebScriptURL.GET_COMMENT_WITH_USERNAME
								.replace(":username", Cookies.getCookie("username")));
					requestComment.setCallback(new RequestCallback() {
						public void onResponseReceived(Request request,
								Response response) {
							List<Comment> comments = JsonConverter.getInstance()
									.deserializeCommentListFromJson(response.getText());
							
							commentPanel.clear();
							
							for (final Comment c : comments) {
								FlowPanel divContent = new FlowPanel();
								FlowPanel divGroup = new FlowPanel();
								FlowPanel btnGroup = new FlowPanel();
								
								Rating rating = new Rating(1, 5);
								rating.setValue(c.getRating());
								Label comment = new Label(c.getComments());
								
								Button updateButton = new Button();
								updateButton.setHTML("<img src=\"./icon/update.svg\" title=\"Update comment " + c.getId()
										+ "\" alt=\"Update trip " + c.getTrip().getId() + "\" />");
								updateButton.addClickHandler(new ClickHandler() {			
									public void onClick(ClickEvent event) {
										HashMap<String, String> parameters = new HashMap<String, String>();
										parameters.put("tripId", String.valueOf(c.getId()));
										parameters.put("id", String.valueOf(c.getId()));
										Main.updateURL(PageName.UPDATE_COMMENT_PAGE, parameters);
									}
								});
								Button deleteButton = new Button();
								deleteButton.setHTML("<img src=\"./icon/delete.svg\" title=\"Delete comment " + c.getId()
										+ "\" alt=\"Delete trip " + c.getTrip().getId() + "\" />");
								deleteButton.addClickHandler(new ClickHandler() {			
									public void onClick(ClickEvent event) {
										showConfirmDialogDelete(c);
									}
								});
								
								divGroup.add(rating);
								divGroup.add(comment);
								
								btnGroup.add(updateButton);
								btnGroup.add(deleteButton);
								
								divContent.add(divGroup);
								divContent.add(btnGroup);

								divGroup.setStyleName(Main.CLASSE_CSS_DIV_GROUP);
								btnGroup.setStyleName(Main.CLASSE_CSS_BTN_V_GROUP);
								divContent.setStyleName(Main.CLASSE_CSS_ELEMENT);
								
								commentPanel.add(divContent);
							}
						}

						public void onError(Request request, Throwable exception) {
							Main.addAlert("Error " + exception.getMessage(), StatusAlert.danger);
						}
					});
					
					try {
						requestComment.send();
					} catch (RequestException e) {
						Main.addAlert("Error " + e.getMessage(), StatusAlert.danger);
					}
				} else {
					Main.addAlert("Get user error (Status " + response.getStatusCode() + ")", StatusAlert.warning);
				}
			}
			
			public void onError(Request request, Throwable exception) {
				Main.addAlert("Error " + exception.getMessage(), StatusAlert.danger);
			}
		});

		try {
			request.send();
		} catch (RequestException e) {
			Main.addAlert("Error " + e.getMessage(), StatusAlert.danger);
		}
	}

	private void showConfirmDialogDelete(Comment comment) {
		super.showConfirmDialog(
				MESSAGES.confirmDeleteCommentTitle(),
				MESSAGES.confirmDeleteComment()
					.replaceAll("%TEXT%", comment.getComments()),
				MESSAGES.deleteCommentSuccess(),
				MESSAGES.deleteCommentError(),
				new RequestBuilder(RequestBuilder.DELETE, 
						WebScriptURL.DELETE_COMMENT
						.replace(":username", Cookies.getCookie("username"))
						.replace(":id", String.valueOf(comment.getId()))
				)
		);
	}
}
