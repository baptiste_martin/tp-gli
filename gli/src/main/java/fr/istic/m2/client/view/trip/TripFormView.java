package fr.istic.m2.client.view.trip;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DatePicker;

import fr.istic.m2.client.Main;
import fr.istic.m2.client.TripMessages;
import fr.istic.m2.client.Main.StatusAlert;
import fr.istic.m2.client.jsonConverter.JsonConverter;
import fr.istic.m2.client.ui.ListTextBox;
import fr.istic.m2.client.url.PageName;
import fr.istic.m2.client.url.WebScriptURL;
import fr.istic.m2.client.view.utils.HandlerBlurMandatory;
import fr.istic.m2.client.view.utils.HandlerChangeRegEx;
import fr.istic.m2.client.view.utils.observer.Observer;
import fr.istic.m2.client.view.utils.reference.ObservableReference;
import fr.istic.m2.shared.model.Car;
import fr.istic.m2.shared.model.Trip;

public class TripFormView extends Composite {
	private final TripMessages MESSAGE = GWT.create(TripMessages.class);
	private final String CLASSE_CSS_FORM = "tripForm";
	private FormPanel form;
	private ListBox car;
	private TextBox start;
	private ListTextBox stops;
	private TextBox end;
	private DatePicker startDate;
	private ListBox nbOfFreeSeats;
	private String id;
	private Map<Long, Car> cars;

	public TripFormView() {
		this(null);
	}
	
	public TripFormView(String id) {
		this.cars = new HashMap<Long, Car>();
		this.form = new FormPanel();
		this.form.setStyleName(CLASSE_CSS_FORM);
		this.id = id;
		
		initPanel();
		fillCar();
		initWidget(form);
	}
	
	private void initPanel() {
		VerticalPanel panel = new VerticalPanel();
		FlowPanel carLine = new FlowPanel();
		FlowPanel startLine = new FlowPanel();
		FlowPanel stopsLine = new FlowPanel();
		FlowPanel endLine = new FlowPanel();
		FlowPanel startDateLine = new FlowPanel();
		FlowPanel nbOfFreeSeatsLine = new FlowPanel();
		
		Label title = new Label((id == null) ? MESSAGE.tripAddTitle() : MESSAGE.tripUpdateTitle());
		title.setStyleName(Main.CLASSE_CSS_TITLE);
		
		Label carLabel = new Label(MESSAGE.car());
		car = new ListBox();
		Label startLabel = new Label(MESSAGE.start());
		start = new TextBox();
		Label stopsLabel = new Label(MESSAGE.stops());
		stops = new ListTextBox();
		Label endLabel = new Label(MESSAGE.end());
		end = new TextBox();
		Label startDateLabel = new Label(MESSAGE.dateStart());
		startDate = new DatePicker();
		Label nbOfFreeSeatsLabel = new Label(MESSAGE.nbOfFreeSeats());
		nbOfFreeSeats = new ListBox();
		for (int i = 0; i < 11; i++) {
			nbOfFreeSeats.addItem(String.valueOf(i), String.valueOf(i));
		}
		final Button button = new Button((id == null) ? MESSAGE.addTrip() : MESSAGE.updateTrip());

		button.setEnabled((id == null) ? false : true);

		final ObservableReference<Boolean> isStartNotEmpty = new ObservableReference<Boolean>(true);
		final ObservableReference<Boolean> isStartRegEx = new ObservableReference<Boolean>(true);
		final ObservableReference<Boolean> isEndNotEmpty = new ObservableReference<Boolean>(true);
		final ObservableReference<Boolean> isEndRegEx = new ObservableReference<Boolean>(true);
		
		Observer observer = new Observer() {			
			public void update() {
				button.setEnabled(isStartNotEmpty.getValue() && isStartRegEx.getValue() &&
						isEndNotEmpty.getValue() && isEndRegEx.getValue());
			}
		};

		isStartNotEmpty.addObserver(observer);
		isStartRegEx.addObserver(observer);
		isEndNotEmpty.addObserver(observer);
		isEndRegEx.addObserver(observer);

		String regEx = "(/w)*";
		
		// Add a handler to check validity of information
		start.addBlurHandler(new HandlerBlurMandatory(start, isStartNotEmpty));
		start.addChangeHandler(new HandlerChangeRegEx(regEx, start, isStartRegEx));
		end.addBlurHandler(new HandlerBlurMandatory(end, isEndNotEmpty));
		end.addChangeHandler(new HandlerChangeRegEx(regEx, end, isEndRegEx));
		
		button.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				RequestBuilder request = (id == null) ?
					new RequestBuilder(RequestBuilder.PUT, 
							WebScriptURL.PUT_TRIP.replace(":username", Cookies.getCookie("username")))
				:
					new RequestBuilder(RequestBuilder.POST, 
							WebScriptURL.POST_TRIP.replace(":username", Cookies.getCookie("username")));
				
				Trip trip = JsonConverter.getInstance().makeTrip();
				if (id != null) {
					trip.setId(Long.valueOf(id));
				}
				trip.setCar(cars.get(Long.valueOf(car.getValue(car.getSelectedIndex()))));
				trip.setStart(start.getText());
				trip.setStops(stops.getValues());
				trip.setEnd(end.getText());
				trip.setStartDate(startDate.getValue());
				trip.setNbOfFreeSeats(nbOfFreeSeats.getSelectedIndex());
				
				request.setHeader("Content-Type", "application/json");
				request.setRequestData(JsonConverter.getInstance().serializeToJson(trip));
				request.setCallback(new RequestCallback() {
					public void onResponseReceived(Request request, Response response) {
						if (response.getStatusCode() == 200) {
							Main.addAlert(((id == null) ? "Add " : "Update") + " trip success", StatusAlert.success);
							Main.updateURL(PageName.TRIP_PAGE);
						} else {
							Main.addAlert(((id == null) ? "Add " : "Update") + 
									" trip error (Status " + response.getStatusCode() + ")", StatusAlert.warning);
						}
					}

					public void onError(Request request, Throwable exception) {
						Main.addAlert("Error " + exception.getMessage(), StatusAlert.danger);
					}
				});

				try {
					request.send();
				} catch (RequestException e) {
					Main.addAlert("Error " + e.getMessage(), StatusAlert.danger);
				}
			}
		});
		
		carLine.add(carLabel);
		carLine.add(car);
		
		startLine.add(startLabel);
		startLine.add(start);
		
		stopsLine.add(stopsLabel);
		stopsLine.add(stops);
		
		endLine.add(endLabel);
		endLine.add(end);
		
		startDateLine.add(startDateLabel);
		startDateLine.add(startDate);
		
		nbOfFreeSeatsLine.add(nbOfFreeSeatsLabel);
		nbOfFreeSeatsLine.add(nbOfFreeSeats);

		panel.add(title);
		panel.add(carLine);
		panel.add(startLine);
		panel.add(stopsLine);
		panel.add(endLine);
		panel.add(startDateLine);
		panel.add(nbOfFreeSeatsLine);
		panel.add(button);
		
		form.add(panel);
	}
	
	private void fillCar() {
		RequestBuilder request = new RequestBuilder(RequestBuilder.GET, 
				WebScriptURL.GET_CARS.replace(":username", Cookies.getCookie("username")));
		
		request.setCallback(new RequestCallback() {
			public void onResponseReceived(Request request, Response response) {
				if (response.getStatusCode() == 200) {
					List<Car> carsList = JsonConverter.getInstance().deserializeCarListFromJson(response.getText());
					for (Car c : carsList) {
						car.addItem(c.getModel(), String.valueOf(c.getId()));
						cars.put(c.getId(), c);
					}
					
					if (id != null) { 
						fillForm();
					}
				} else {
					Main.addAlert("Get cars error (Status " + response.getStatusCode() + ")", StatusAlert.warning);
				}
			}

			public void onError(Request request, Throwable exception) {
				Main.addAlert("Error " + exception.getMessage(), StatusAlert.danger);
			}
		});

		try {
			request.send();
		} catch (RequestException e) {
			Main.addAlert("Error " + e.getMessage(), StatusAlert.danger);
		}
	}
	
	private void fillForm() {
		RequestBuilder request = new RequestBuilder(RequestBuilder.GET, 
				WebScriptURL.GET_TRIP.replace(":id", id));
		
		request.setCallback(new RequestCallback() {
			public void onResponseReceived(Request request, Response response) {
				if (response.getStatusCode() == 200) {
					Trip trip = JsonConverter.getInstance().deserializeTripFromJson(response.getText());
					for (int i = 0; i < car.getItemCount(); i++) {
						if (car.getValue(i).equals(String.valueOf(trip.getCar().getId()))) {
							car.setSelectedIndex(i);
						}
					}
					start.setValue(trip.getStart());
					if (trip.getStops().isEmpty()) {
						trip.getStops().add("");
					}
					stops.setValues(trip.getStops());
					end.setValue(trip.getEnd());
					startDate.setValue(trip.getStartDate());
					nbOfFreeSeats.setSelectedIndex(trip.getNbOfFreeSeats());
				} else {
					Main.addAlert("Get trip error (Status " + response.getStatusCode() + ")", StatusAlert.warning);
				}
			}

			public void onError(Request request, Throwable exception) {
				Main.addAlert("Error " + exception.getMessage(), StatusAlert.danger);
			}
		});

		try {
			request.send();
		} catch (RequestException e) {
			Main.addAlert("Error " + e.getMessage(), StatusAlert.danger);
		}
	}
}
