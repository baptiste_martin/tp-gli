package fr.istic.m2.client.url;

/**
 * Interface management application REST web script URL
 */
public interface WebScriptURL {
	// Root URL of server
	public static final String URL_SERVER = "http://127.0.0.1:8888/rest";
	
	// Login URL
	public static final String POST_LOGIN = URL_SERVER + "/person/login";
	
	// User URLs
	public static final String GET_USER = URL_SERVER + "/person/:username";
	public static final String PUT_USER = URL_SERVER + "/person";
	public static final String POST_USER = URL_SERVER + "/person";
	
	// Car URLs
	public static final String GET_CARS = URL_SERVER + "/car/driver/:username";
	public static final String GET_CAR = URL_SERVER + "/car/:id";
	public static final String PUT_CAR = URL_SERVER + "/car";
	public static final String POST_CAR = URL_SERVER + "/car";
	public static final String DELETE_CAR = URL_SERVER + "/car/:id";
	
	// Trip URLs
	public static final String GET_TRIPS = URL_SERVER + "/trip/all";
	public static final String GET_TRIPS_WITH_USERNAME = URL_SERVER + "/trip/with/:username";
	public static final String GET_TRIP = URL_SERVER + "/trip/:id";
	public static final String PUT_TRIP = URL_SERVER + "/trip/:username";
	public static final String POST_TRIP = URL_SERVER + "/trip/:username";
	public static final String GET_SEARCH_TRIP = URL_SERVER + "/trip/search/:start/:end";
	public static final String GET_SEARCH_TRIP_WITH_USERNAME = URL_SERVER + "/trip/search/:username/:start/:end";
	public static final String POST_JOIN_TRIP = URL_SERVER + "/trip/join/:username";
	public static final String POST_UNJOIN_TRIP =  URL_SERVER + "/trip/unjoin/:username";
	public static final String DELETE_TRIP = URL_SERVER + "/trip/:username/:id";

	// Comment URLs
	public static final String GET_COMMENT = URL_SERVER + "/comment/:id";
	public static final String GET_COMMENT_WITH_TRIP = URL_SERVER + "/comment/withTrip/:id";
	public static final String GET_COMMENT_WITH_USERNAME = URL_SERVER + "/comment/withPerson/:username";
	public static final String PUT_COMMENT = URL_SERVER + "/comment/:username";
	public static final String POST_COMMENT = URL_SERVER + "/comment/:username";
	public static final String DELETE_COMMENT = URL_SERVER + "/comment/:username/:id";
}
