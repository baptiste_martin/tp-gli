package fr.istic.m2.client.jsonConverter;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

import fr.istic.m2.shared.model.Comment;
import fr.istic.m2.shared.model.Result;
import fr.istic.m2.shared.model.Car;
import fr.istic.m2.shared.model.Person;
import fr.istic.m2.shared.model.Trip;

public class JsonConverter {
	public interface JsonFactory extends AutoBeanFactory {
		AutoBean<Person> person();
		AutoBean<Car> car();
		AutoBean<Trip> trip();
		AutoBean<Comment> comment();
		AutoBean<Result> result();
	}

	private class ClassConverter {
		@SuppressWarnings("unchecked")
		private <T> T makeClass(Class<T> type) {
			if (type.equals(Person.class))
				return (T) factory.person().as();
			else if (type.equals(Car.class))
				return (T) factory.car().as();
			else if (type.equals(Trip.class))
				return (T) factory.trip().as();
			else if (type.equals(Comment.class))
				return (T) factory.comment().as();

			return null;
		}
		
		private <T> String serializeToJson(T object) {
			return AutoBeanCodex.encode(AutoBeanUtils.getAutoBean(object)).getPayload();
		}
		
		private <T> T deserializeObjectFromJson(Class<T> type, String json) {
			return AutoBeanCodex.decode(factory, type, json).as();
		}
		
		@SuppressWarnings("unchecked")
		private <T> List<T> deserializeListFromJson(Class<T> type, String payload, String json) {
			Result r = AutoBeanCodex.decode(factory, Result.class, "{\"" + payload + "\": " + json + "}").as();
			
			if (type.equals(Person.class))
				return (List<T>) r.getPersons();
			else if (type.equals(Car.class))
				return (List<T>) r.getCars();
			else if (type.equals(Trip.class))
				return (List<T>) r.getTrips();
			else if (type.equals(Comment.class))
				return (List<T>) r.getComments();
			
			return null;
		}
	}

	private JsonConverter() {
		classConverter = new ClassConverter();
	}
	
	private final static JsonConverter INSTANCE = new JsonConverter();
	private static ClassConverter classConverter;

	// Instantiate the factory
	JsonFactory factory = GWT.create(JsonFactory.class);

	// In non-GWT code, use AutoBeanFactorySource.create(MyFactory.class);

	public Person makePerson() {
		return classConverter.makeClass(Person.class);
	}

    public String serializeToJson(Person person) {
		return classConverter.serializeToJson(person);
	}

    public Person deserializePersonFromJson(String json) {
		return classConverter.deserializeObjectFromJson(Person.class, json);
	}
	
	public Car makeCar() {
		return classConverter.makeClass(Car.class);
	}

	public String serializeToJson(Car car) {
		return classConverter.serializeToJson(car);
	}

	public Car deserializeCarFromJson(String json) {
		return classConverter.deserializeObjectFromJson(Car.class, json);
	}
	
	public List<Car> deserializeCarListFromJson(String json) {
		return classConverter.deserializeListFromJson(Car.class, "cars", json);
	}
	
	public Trip makeTrip() {
		return classConverter.makeClass(Trip.class);
	}

	public String serializeToJson(Trip trip) {
		return classConverter.serializeToJson(trip);
	}

	public Trip deserializeTripFromJson(String json) {
		return classConverter.deserializeObjectFromJson(Trip.class, json);
	}
	
	public List<Trip> deserializeTripListFromJson(String json) {
		return classConverter.deserializeListFromJson(Trip.class, "trips", json);
	}
	
	public Comment makeComment() {
		return classConverter.makeClass(Comment.class);
	}

	public String serializeToJson(Comment comment) {
		return classConverter.serializeToJson(comment);
	}

	public Comment deserializeCommentFromJson(String json) {
		return classConverter.deserializeObjectFromJson(Comment.class, json);
	}
	
	public List<Comment> deserializeCommentListFromJson(String json) {
		return classConverter.deserializeListFromJson(Comment.class, "comments", json);
	}

	public static JsonConverter getInstance() {
		return INSTANCE;
	}
}
