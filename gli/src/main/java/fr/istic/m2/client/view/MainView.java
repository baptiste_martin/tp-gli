package fr.istic.m2.client.view;

import java.util.HashMap;

import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TabPanel;

import fr.istic.m2.client.Main;
import fr.istic.m2.client.composite.AbstractComposite;

public class MainView extends Composite {
	private TabPanel panel;
	private HashMap<Integer, String> namesOfPages;
	private HashMap<Integer, AbstractComposite> pages;
	private int nbOfPages;
	
	public MainView() {
		nbOfPages = 0;
		namesOfPages = new HashMap<Integer, String>();
		pages = new HashMap<Integer, AbstractComposite>();
		panel = new TabPanel();
		panel.setAnimationEnabled(true);
		
		panel.addSelectionHandler(new SelectionHandler<Integer>() {			
			public void onSelection(SelectionEvent<Integer> event) {
				Main.updateURL(namesOfPages.get(event.getSelectedItem()));
			}
		});
	}
	
	public void addPage(String tabName, AbstractComposite page) {
		panel.add(page, tabName.replaceFirst("/", ""));
		namesOfPages.put(nbOfPages, tabName);
		pages.put(nbOfPages, page);
		nbOfPages++;
	}
	
	public void validatePage(int width) {
		panel.setWidth(Integer.toString(width));
	      
		initWidget(panel);
	}
	
	public void selectPage(String tabName){
		for (Integer i : namesOfPages.keySet()) {
			if (namesOfPages.get(i).equals(tabName)) {
				panel.selectTab(i);
				pages.get(i).showPage();
				return;
			}
		}
		throw new IllegalArgumentException(tabName + " doesn't exist.");
	}
}
