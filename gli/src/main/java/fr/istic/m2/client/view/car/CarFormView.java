package fr.istic.m2.client.view.car;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.istic.m2.client.Main;
import fr.istic.m2.client.CarMessages;
import fr.istic.m2.client.Main.StatusAlert;
import fr.istic.m2.client.jsonConverter.JsonConverter;
import fr.istic.m2.client.url.PageName;
import fr.istic.m2.client.url.WebScriptURL;
import fr.istic.m2.client.view.utils.HandlerBlurMandatory;
import fr.istic.m2.client.view.utils.observer.Observer;
import fr.istic.m2.client.view.utils.reference.ObservableReference;
import fr.istic.m2.shared.model.Car;
import fr.istic.m2.shared.model.Person;

public class CarFormView extends Composite {
	private final CarMessages MESSAGES = GWT.create(CarMessages.class);
	private final String CLASSE_CSS_FORM = "carForm";
	private FormPanel form;
	private TextBox model;
	private ListBox nbSeats;
	private CheckBox hasAC;
	private CheckBox isSmoker;
	private CheckBox hasRadio;
	private String id;
	
	public CarFormView() {
		this(null);
	}
	
	public CarFormView(String id) {
		this.form = new FormPanel();
		this.form.setStyleName(CLASSE_CSS_FORM);
		this.id = id;
		
		initPanel();
		if (id != null) fillForm();
		initWidget(form);
	}
	
	private void initPanel() {
		VerticalPanel panel = new VerticalPanel();
		final FlowPanel nameLine = new FlowPanel();
		final FlowPanel nbSeatsLine = new FlowPanel();
		
		Label title = new Label((id == null) ? MESSAGES.carAddTitle() : MESSAGES.carUpdateTitle());
		title.setStyleName(Main.CLASSE_CSS_TITLE);
		
		final Label modelLabel = new Label(MESSAGES.model());
		model = new TextBox();
		
		final Label nbSeatsLabel = new Label(MESSAGES.numberOfSeats());
		nbSeats = new ListBox();
		for (int i = 2; i < 11; i++) {
			nbSeats.addItem(String.valueOf(i), String.valueOf(i));
		}
		
		hasAC = new CheckBox(MESSAGES.hasAC());
		isSmoker = new CheckBox(MESSAGES.isSmoker());
		hasRadio = new CheckBox(MESSAGES.hasRadio());
		
		final Button submit = new Button((id == null) ? MESSAGES.addCar() : MESSAGES.updateCar());
		
		submit.setEnabled((id == null) ? false : true);
		
		final ObservableReference<Boolean> isNameNotEmpty = new ObservableReference<Boolean>(true);
		
		Observer observer = new Observer() {			
			public void update() {
				submit.setEnabled(isNameNotEmpty.getValue());
			}
		};

		isNameNotEmpty.addObserver(observer);

		// Add a handler to check validity of information
		model.addBlurHandler(new HandlerBlurMandatory(model, isNameNotEmpty));
		
		submit.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				Car car = JsonConverter.getInstance().makeCar(); // new Car();
				Person driver = JsonConverter.getInstance().makePerson(); // new Person();
				if (id != null) {
					car.setId(Long.valueOf(id));
				}
				driver.setUsername(Cookies.getCookie("username"));
				car.setModel(model.getText());
				car.setDriver(driver);
				car.setNbOfSeats(nbSeats.getSelectedIndex() + 2);
				car.setHasAC(hasAC.getValue());
				car.setIsSmoker(isSmoker.getValue());
				car.setHasRadio(hasRadio.getValue());
				
				RequestBuilder request = (id == null) ?
					new RequestBuilder(RequestBuilder.PUT, WebScriptURL.PUT_CAR)
				:
					new RequestBuilder(RequestBuilder.POST, WebScriptURL.POST_CAR);
				
				request.setHeader("Content-Type", "application/json");
				request.setRequestData(JsonConverter.getInstance().serializeToJson(car));
				request.setCallback(new RequestCallback() {						
					public void onResponseReceived(Request request, Response response) {
						if (response.getStatusCode() == 200) {
							Main.addAlert(((id == null) ? "Add" : "Update") + " car success", StatusAlert.success);
							Main.updateURL(PageName.CAR_PAGE);
						} else {
							Main.addAlert(((id == null) ? "Add" : "Update") + " car error (Status " + 
									response.getStatusCode() + ")", StatusAlert.warning);
						}
					}
					
					public void onError(Request request, Throwable exception) {
						Main.addAlert("Error " + exception.getMessage(), StatusAlert.danger);
					}
				});
				
				try {
					request.send();
				} catch (RequestException e) {
					Main.addAlert("Error " + e.getMessage(), StatusAlert.danger);
				}
			}
		});
		
		nameLine.add(modelLabel);
		nameLine.add(model);
		
		nbSeatsLine.add(nbSeatsLabel);
		nbSeatsLine.add(nbSeats);

		panel.add(title);
		panel.add(nameLine);
		panel.add(nbSeatsLine);
		panel.add(hasAC);
		panel.add(isSmoker);
		panel.add(hasRadio);
		panel.add(submit);
		
		form.add(panel);
	}
	
	private void fillForm() {
		RequestBuilder request = new RequestBuilder(RequestBuilder.GET, 
				WebScriptURL.GET_CAR.replace(":id", id));
		
		request.setCallback(new RequestCallback() {
			public void onResponseReceived(Request request, Response response) {
				if (200 == response.getStatusCode()) {
					Car car = JsonConverter.getInstance().deserializeCarFromJson(response.getText());
					
					model.setText(car.getModel());
					nbSeats.setSelectedIndex(car.getNbOfSeats() - 2);
					hasAC.setValue(car.getHasAC());
					isSmoker.setValue(car.getIsSmoker());
					hasRadio.setValue(car.getHasRadio());
				} else {
					Main.addAlert("Get car error (Status " + response.getStatusCode() + ")", StatusAlert.warning);
				}
			}

			public void onError(Request request, Throwable exception) {
				Main.addAlert("Error " + exception.getMessage(), StatusAlert.danger);
			}
		});

		try {
			request.send();
		} catch (RequestException e) {
			Main.addAlert("Error " + e.getMessage(), StatusAlert.danger);
		}
	}
}
