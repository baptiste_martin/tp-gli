package fr.istic.m2.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.EntityEnclosingMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.io.IOUtils;

/**
 * Servlet implementation class ProxyServlet
 */
public class ProxyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String SERVER_PATH_ROOT = "http://localhost:8080/";

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doMethod(request, response, new GetMethod(getPathRequest(request)));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PostMethod post = new PostMethod(getPathRequest(request));
		addBody(request, post);
		doMethod(request, response, post);
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doMethod(request, response, new DeleteMethod(getPathRequest(request)));
	}
	
	/**
	 * @see HttpServlet#doPut(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PutMethod put = new PutMethod(getPathRequest(request));
		addBody(request, put);
		doMethod(request, response, put);
	}
	
	private void addBody(HttpServletRequest request, EntityEnclosingMethod method) throws UnsupportedEncodingException, IOException {
		StringRequestEntity requestEntity = new StringRequestEntity(
				IOUtils.toString(request.getInputStream(), request.getCharacterEncoding()),
				"application/json",
			    request.getCharacterEncoding());
		method.setRequestEntity(requestEntity);
	}
	
	private void doMethod(HttpServletRequest request, HttpServletResponse response, HttpMethod method) throws IOException {
		HttpClient httpClient = new HttpClient();
		BufferedReader reader = null;
		
		try {
			method.addRequestHeader("Content-type", "application/json");
	        int responseStatus = httpClient.executeMethod(method);
	        
	        response.setStatus(responseStatus);
	    	response.setContentType("application/json");
	    	
	    	reader = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
	    	String line;
	    	
	    	while ((line = reader.readLine()) != null) {
	    		response.getWriter().append(line);
	    	}
	    } catch (Exception ex) {
	    	response.setStatus(500);
	    	response.setContentType("application/json");
	    	response.getWriter().append("Error : " + ex.getMessage() + ' ' + method.getURI());
	    	response.getWriter().append("\nStack Trace :\n");
	    	for (StackTraceElement stackTraceElement : ex.getStackTrace()) {
	    		response.getWriter().append(stackTraceElement.toString() + '\n');
	    	}
	    } finally {
	    	reader.close();
	    	method.releaseConnection();
	    }
	}
	
	private String getPathRequest(HttpServletRequest request) throws URIException {
		String uri = request.getRequestURI();
		int beginIndex = uri.indexOf("rest/");
		String resources = uri.substring(beginIndex);
		String params = "?";
		Iterator<String> it = request.getParameterMap().keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			for (int i = 0; i < request.getParameterMap().get(key).length; i++) {
				String value = request.getParameterMap().get(key)[i];
				params += key + '=' + URIUtil.encodeAll(value) + 
						((it.hasNext() && request.getParameterMap().get(key).length > i - 1) ? '&' : "");
			}
		}
		

		return SERVER_PATH_ROOT + resources + params;
	}
}
