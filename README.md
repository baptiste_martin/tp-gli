# TP GWT #
Baptiste MARTIN    
Fernando DIAZ ANGELES ORTEGA

Ce projet sert comme client GWT pour le service de covoiturage, il utilise le backend de TAA (API Rest + Persistance JPA) disponible sur:

>>[https://bitbucket.org/baptiste_martin/tp-taa](https://bitbucket.org/baptiste_martin/tp-taa)

La classe proxy fr.istic.m2.server.ProxyServlet permet de rediriger les requêtes vers le backend et de séparer les deux projets pour pouvoir les déployer sur différents serveur.

La classe de description des webscript fr.istic.m2.client.url.WebScriptURL définit l'url root du serveur ***http://NOM_DU_SERVER/gli***.

Pour deployer le projet il faut faire *mvn install* et copier le **target/gli.war** dans **TOMCAT_PATH/webapps/**

Puis on peut y acceder sur ***http://NOM_DU_SERVER/gli*** et ça marche ;-)